package com.owaiojek.eklanku.mSend;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.home.submenu.TopUpActivity;
import com.owaiojek.eklanku.model.Driver;
import com.owaiojek.eklanku.model.Fitur;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.book.RequestSendRequestJson;
import com.owaiojek.eklanku.utils.Log;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

import static com.owaiojek.eklanku.mSend.SendActivity.FITUR_KEY;

public class AddDetailSendActivity extends AppCompatActivity implements Validator.ValidationListener {
    @BindView(R.id.mSend_distance)
    TextView distanceText;
    @BindView(R.id.mSend_price)
    TextView priceText;
  /*  @BindView(R.id.mSend_paymentGroup)
    RadioGroup paymentGroup;*/
    @BindView(R.id.mSend_mPayPayment)
    TextView mPayButton;
    @BindView(R.id.mSend_cashPayment)
    TextView cashButton;
    @BindView(R.id.mSend_topUp)
    Button topUpButton;
    @BindView(R.id.mSend_mPayBalance)
    TextView mPayBalanceText;
    @BindView(R.id.mSend_order)
    Button orderButton;
    @NotEmpty
    @BindView(R.id.mSend_goods_description)
    EditText goodsDescription;
    @NotEmpty
    @BindView(R.id.mSend_sender_name)
    EditText senderName;
    @NotEmpty
    @BindView(R.id.mSend_sender_phone)
    EditText senderPhone;
    @NotEmpty
    @BindView(R.id.mSend_receiver_name)
    EditText receiverName;
    @NotEmpty
    @BindView(R.id.mSend_receiver_phone)
    EditText receiverPhone;
    @BindView(R.id.discountText)
    TextView discountText;

    private double distance;
    private long price;
    private LatLng pickUpLatLang;
    private LatLng destinationLatLang;
    private String pickup;
    private String destination;
    private ArrayList<Driver> driverAvailable;
    private double timeDistance = 0;
//    DiskonMpay diskonMpay;
    private long mpayBalance;
    private boolean isCash = false,isSalod = false;
    private Fitur fitur;
    Realm realm;
    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_add_detail);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);


        realm = Realm.getDefaultInstance();
        User userLogin = MangJekApplication.getInstance(this).getLoginUser();
        mpayBalance = userLogin.getmPaySaldo();
//        diskonMpay = MangJekApplication.getInstance(this).getDiskonMpay();
        Intent intent = getIntent();
        if(intent.hasExtra("distance")){
            distance = intent.getDoubleExtra("distance", 0);
            price = intent.getLongExtra("price", 0);
            pickUpLatLang = intent.getParcelableExtra("pickup_latlng");
            destinationLatLang = intent.getParcelableExtra("destination_latlng");
            pickup = intent.getStringExtra("pickup");
            destination = intent.getStringExtra("destination");
            timeDistance = intent.getDoubleExtra("time_distance", 0);
            driverAvailable = (ArrayList<Driver>)intent.getSerializableExtra("driver");
            int selectedFitur = intent.getIntExtra(FITUR_KEY, -1);

            if (selectedFitur != -1)
                fitur = realm.where(Fitur.class).equalTo("idFitur", selectedFitur).findFirst();

            discountText.setText("Diskon "+fitur.getDiskon()+" jika menggunakan SALDO");
        }

        long biayaTotal = price;
        String formattedTotal = NumberFormat.getNumberInstance(Locale.US).format(biayaTotal);
        String formattedText = String.format(Locale.US, "Rp. %s ,-", formattedTotal);
        priceText.setText(formattedText);

        float km = ((float) distance);
        String format = String.format(Locale.US, "(%.1f Km)", km);
        distanceText.setText(format);
        if(mpayBalance < (price * fitur.getBiayaAkhir())){
            mPayButton.setEnabled(false);
            selectCash();
        }else {
            mPayButton.setEnabled(true);
            selectSaldo();
        }
        cashButton.setSelected(true);
        mPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectSaldo();
            }
        });
        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCash();
            }
        });

       // cashButton.setChecked(true);
       /* paymentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (paymentGroup.getCheckedRadioButtonId()) {
                    case R.id.mSend_mPayPayment:
                        long biayaTotal = (long)(price * fitur.getBiayaAkhir());
                        String formattedTotal = NumberFormat.getNumberInstance(Locale.US).format(biayaTotal);
                        String formattedText = String.format(Locale.US, "Rp. %s ,-", formattedTotal);
                        priceText.setText(formattedText);
                        break;
                    case R.id.mSend_cashPayment:
                        biayaTotal = price;
                        formattedTotal = NumberFormat.getNumberInstance(Locale.US).format(biayaTotal);
                        formattedText = String.format(Locale.US, "Rp. %s ,-", formattedTotal);
                        priceText.setText(formattedText);
                        break;
                    default:
                        biayaTotal = price;
                        formattedTotal = NumberFormat.getNumberInstance(Locale.US).format(biayaTotal);
                        formattedText = String.format(Locale.US, "Rp. %s ,-", formattedTotal);
                        priceText.setText(formattedText);
                        break;
                }
            }
        });*/


        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
               // onOrderButtonClick();

            }
        });

        topUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TopUpActivity.class));
            }
        });

    }

        private void onOrderButtonClick() {
      //  switch (paymentGroup.getCheckedRadioButtonId()) {
            if (isSalod) {
                if (driverAvailable.isEmpty()) {
                    AlertDialog dialog = new AlertDialog.Builder(AddDetailSendActivity.this)
                            .setMessage("Mohon maaf, tidak ada driver disekitar.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .create();
                    dialog.show();
                } else {
                    RequestSendRequestJson param = new RequestSendRequestJson();
                    User userLogin = MangJekApplication.getInstance(this).getLoginUser();
                    param.idPelanggan = userLogin.getId();
                    param.orderFitur = "5";
                    param.startLatitude = pickUpLatLang.latitude;
                    param.startLongitude = pickUpLatLang.longitude;
                    param.endLatitude = destinationLatLang.latitude;
                    param.endLongitude = destinationLatLang.longitude;
                    param.jarak = this.distance;
                    param.harga = (long) (this.price * fitur.getBiayaAkhir());
                    param.alamatAsal = pickup;
                    param.alamatTujuan = destination;
                    param.namaPengirim = senderName.getText().toString();
                    param.teleponPengirim = senderPhone.getText().toString();
                    param.namaPenerima = receiverName.getText().toString();
                    param.teleponPenerima = receiverPhone.getText().toString();
                    param.namaBarang = goodsDescription.getText().toString();


                    Log.e("M-PAY", "used");
                    param.pakaiMpay = 1;

                    Intent intent = new Intent(AddDetailSendActivity.this, SendWaitingActivity.class);
                    intent.putExtra(SendWaitingActivity.REQUEST_PARAM, param);
                    intent.putExtra(SendWaitingActivity.DRIVER_LIST, (ArrayList) driverAvailable);
                    intent.putExtra("time_distance", timeDistance);
                    startActivity(intent);
                }
            }


            if(isCash) {
                if (driverAvailable.isEmpty()) {
                    AlertDialog dialog = new AlertDialog.Builder(AddDetailSendActivity.this)
                            .setMessage("Mohon maaf, tidak ada driver disekitar.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .create();
                    dialog.show();
                } else {
                    RequestSendRequestJson param = new RequestSendRequestJson();
                    User userLogin = MangJekApplication.getInstance(this).getLoginUser();
                    param.idPelanggan = userLogin.getId();
                    param.orderFitur = "5";
                    param.startLatitude = pickUpLatLang.latitude;
                    param.startLongitude = pickUpLatLang.longitude;
                    param.endLatitude = destinationLatLang.latitude;
                    param.endLongitude = destinationLatLang.longitude;
                    param.jarak = this.distance;
                    param.harga = this.price;
                    param.alamatAsal = pickup;
                    param.alamatTujuan = destination;
                    param.namaPengirim = senderName.getText().toString();
                    param.teleponPengirim = senderPhone.getText().toString();
                    param.namaPenerima = receiverName.getText().toString();
                    param.teleponPenerima = receiverPhone.getText().toString();
                    param.namaBarang = goodsDescription.getText().toString();

                    Log.e("M-PAY", "not using m pay");
                    param.pakaiMpay = 0;


                    Intent intent = new Intent(AddDetailSendActivity.this, SendWaitingActivity.class);
                    intent.putExtra(SendWaitingActivity.REQUEST_PARAM, param);
                    intent.putExtra(SendWaitingActivity.DRIVER_LIST, (ArrayList) driverAvailable);
                    intent.putExtra("time_distance", timeDistance);
                    startActivity(intent);
                }
            }

      //  }
    }

    @Override
    protected void onResume() {
        super.onResume();
        User userLogin = MangJekApplication.getInstance(this).getLoginUser();
        String formattedText = String.format(Locale.US, "Rp. %s ,-",
                NumberFormat.getNumberInstance(Locale.US).format(userLogin.getmPaySaldo()));

        mPayBalanceText.setText(formattedText);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void selectCash(){
        isCash = true;
        isSalod = false;
       long biayaTotal = price;
       String formattedTotal = NumberFormat.getNumberInstance(Locale.US).format(biayaTotal);
       String formattedText = String.format(Locale.US, "Rp. %s ,-", formattedTotal);
        priceText.setText(formattedText);
        mPayButton.setSelected(false);
        cashButton.setSelected(true);
        cashButton.setBackground(getResources().getDrawable(R.drawable.roundcor));
        mPayButton.setBackground(null);
    }

    private void selectSaldo(){
        isSalod = true;
        isCash = false;
        long biayaTotal = (long)(price * fitur.getBiayaAkhir());
        String formattedTotal = NumberFormat.getNumberInstance(Locale.US).format(biayaTotal);
        String formattedText = String.format(Locale.US, "Rp. %s ,-", formattedTotal);
        priceText.setText(formattedText);
        mPayButton.setSelected(true);
        cashButton.setSelected(false);
        mPayButton.setBackground(getResources().getDrawable(R.drawable.roundcor));
        cashButton.setBackground(null);
    }

    @Override
    public void onValidationSucceeded() {
        onOrderButtonClick();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error: errors){
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
