package com.owaiojek.eklanku.mSend;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.FCMHelper;
import com.owaiojek.eklanku.api.MapDirectionAPI;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.BookService;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.gmap.directions.Directions;
import com.owaiojek.eklanku.gmap.directions.Route;
import com.owaiojek.eklanku.mRideCar.InProgressActivity;
import com.owaiojek.eklanku.mRideCar.WaitingActivity;
import com.owaiojek.eklanku.model.Driver;
import com.owaiojek.eklanku.model.Transaksi;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.book.CheckStatusTransaksiRequest;
import com.owaiojek.eklanku.model.json.book.CheckStatusTransaksiResponse;
import com.owaiojek.eklanku.model.json.book.RequestSendRequestJson;
import com.owaiojek.eklanku.model.json.book.RequestSendResponseJson;
import com.owaiojek.eklanku.model.json.fcm.CancelBookRequestJson;
import com.owaiojek.eklanku.model.json.fcm.CancelBookResponseJson;
import com.owaiojek.eklanku.model.json.fcm.DriverRequest;
import com.owaiojek.eklanku.model.json.fcm.DriverResponse;
import com.owaiojek.eklanku.model.json.fcm.FCMMessage;
import com.owaiojek.eklanku.utils.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.owaiojek.eklanku.config.General.FCM_KEY;
import static com.owaiojek.eklanku.model.FCMType.ORDER;
import static com.owaiojek.eklanku.model.json.fcm.DriverResponse.REJECT;

/**
 * Created by bradhawk on 10/17/2016.
 */

public class SendWaitingActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String REQUEST_PARAM = "RequestParam";
    public static final String DRIVER_LIST = "DriverList";

    private List<Driver> driverList;
    private RequestSendRequestJson param;

    private DriverRequest request;
    private int currentLoop;
    private LatLng pickUpLatLang;
    private LatLng destinationLatLang;
    private GoogleMap gMap;
    private Polyline directionLine;
    private Marker pickUpMarker;
    private Marker destinationMarker;
    AppCompatActivity activity;

    private Driver driver;

    private double timeDistance;
    Transaksi transaksi;
    Thread thread;
    boolean threadRun = true;

    @BindView(R.id.waiting_cancel)
    Button cancelButton;


    private okhttp3.Callback updateRouteCallback = new okhttp3.Callback() {
        @Override
        public void onFailure(okhttp3.Call call, IOException e) {

        }

        @Override
        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
            if (response.isSuccessful()) {
                final String json = response.body().string();
                final long distance = MapDirectionAPI.getDistance(SendWaitingActivity.this, json);
                final long time = MapDirectionAPI.getTimeDistance(SendWaitingActivity.this, json);
                if (distance >= 0) {
                    SendWaitingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateLineDestination(json);
                        }
                    });
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);
        ButterKnife.bind(this);

        activity = this;

        param = (RequestSendRequestJson) getIntent().getSerializableExtra(REQUEST_PARAM);
        driverList = (List<Driver>) getIntent().getSerializableExtra(DRIVER_LIST);

        timeDistance = getIntent().getDoubleExtra("time_distance", 0);
        currentLoop = 0;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.waiting_car_mapView);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        pickUpLatLang = new LatLng(param.startLatitude,param.startLongitude);
        destinationLatLang = new LatLng(param.endLatitude,param.endLongitude);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(request != null){
                    cancelOrder();
                }


            }
        });

        sendRequestTransaksi();
    }

    private void requestRoute() {
        if (pickUpLatLang != null && destinationLatLang != null) {
            MapDirectionAPI.getDirection(pickUpLatLang, destinationLatLang).enqueue(updateRouteCallback);
        }
    }

    private void sendRequestTransaksi() {
        User loginUser = MangJekApplication.getInstance(this).getLoginUser();
        final BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());

        service.requestTransMSend(param).enqueue(new Callback<RequestSendResponseJson>() {
            @Override
            public void onResponse(Call<RequestSendResponseJson> call, Response<RequestSendResponseJson> response) {
                if (response.isSuccessful()) {
                    buildDriverRequest(response.body());
//                    fcmBroadcast(currentLoop);


                    thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < driverList.size(); i++) {
                                if (threadRun) {
                                    fcmBroadcast(currentLoop);
                                }
                            }
                            try {
                                Thread.sleep(25000);
//                                    if(!threadRun){
//                                        thread.stop();
//                                    }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (threadRun) {
                                CheckStatusTransaksiRequest param = new CheckStatusTransaksiRequest();
                                param.setIdTransaksi(transaksi.getId());
                                service.checkStatusTransaksi(param).enqueue(new Callback<CheckStatusTransaksiResponse>() {
                                    @Override
                                    public void onResponse(Call<CheckStatusTransaksiResponse> call, Response<CheckStatusTransaksiResponse> response) {
                                        if(response.isSuccessful()) {
                                            CheckStatusTransaksiResponse checkStatus = response.body();
                                            if(checkStatus.isStatus()) {
                                                Intent intent = new Intent(activity, InProgressActivity.class);
                                                intent.putExtra("driver", checkStatus.getListDriver().get(0));
                                                intent.putExtra("request", request);
                                                intent.putExtra("time_distance", timeDistance);
                                                startActivity(intent);
                                            } else {
                                                Log.e("DRIVER STATUS", "Driver not found!");
                                                activity.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(SendWaitingActivity.this, "Belum mendapatkan driver, silahkan coba lagi.", Toast.LENGTH_LONG).show();
                                                    }
                                                });

                                                finish();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CheckStatusTransaksiResponse> call, Throwable t) {
                                        Log.e("DRIVER STATUS", "Driver not found!");
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(SendWaitingActivity.this, "Belum mendapatkan driver, silahkan coba lagi.", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                        finish();
                                    }
                                });
                            }
                        }
                    });
                    thread.start();


                }
            }

            @Override
            public void onFailure(Call<RequestSendResponseJson> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void cancelOrder(){
        User loginUser = MangJekApplication.getInstance(SendWaitingActivity.this).getLoginUser();
        CancelBookRequestJson request = new CancelBookRequestJson();

        request.id = loginUser.getId();
        request.id_transaksi = this.request.getIdTransaksi();
        request.id_driver = "D0";

        Log.d("id_transaksi_cancel", this.request.getIdTransaksi());
        UserService service = ServiceGenerator.createService(UserService.class, loginUser.getEmail(), loginUser.getPassword());
        service.cancelOrder(request).enqueue(new Callback<CancelBookResponseJson>() {
            @Override
            public void onResponse(Call<CancelBookResponseJson> call, Response<CancelBookResponseJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().mesage.equals("order canceled")) {
                        Toast.makeText(SendWaitingActivity.this, "Order Canceled!", Toast.LENGTH_SHORT).show();
                        threadRun = false;
                        finish();
                    } else {
                        Toast.makeText(SendWaitingActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CancelBookResponseJson> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(SendWaitingActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });

        DriverResponse response = new DriverResponse();
        response.type = ORDER;
        response.setIdTransaksi(this.request.getIdTransaksi());
        response.setResponse(REJECT);

        FCMMessage message = new FCMMessage();
        message.setTo(driverList.get(currentLoop-1).getRegId());
        message.setData(response);


        FCMHelper.sendMessage(FCM_KEY, message).enqueue(new okhttp3.Callback() {
            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                Log.e("CANCEL REQUEST", "sent");
                threadRun = false;

            }

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                e.printStackTrace();
                Log.e("CANCEL REQUEST", "failed");
            }
        });
    }

    private void buildDriverRequest(RequestSendResponseJson response) {
        transaksi = response.getData().get(0);
        User loginUser = MangJekApplication.getInstance(this).getLoginUser();
        if (request == null) {
            request = new DriverRequest();
            request.setIdTransaksi(transaksi.getId());
            request.setIdPelanggan(transaksi.getIdPelanggan());
            request.setRegIdPelanggan(loginUser.getRegId());
            request.setOrderFitur(transaksi.getOrderFitur());
            request.setStartLatitude(transaksi.getStartLatitude());
            request.setStartLongitude(transaksi.getStartLongitude());
            request.setEndLatitude(transaksi.getEndLatitude());
            request.setEndLongitude(transaksi.getEndLongitude());
            request.setJarak(transaksi.getJarak());
            request.setHarga(transaksi.getHarga());
            request.setWaktuOrder(transaksi.getWaktuOrder());
            request.setAlamatAsal(transaksi.getAlamatAsal());
            request.setAlamatTujuan(transaksi.getAlamatTujuan());
            request.setKodePromo(transaksi.getKodePromo());
            request.setKreditPromo(transaksi.getKreditPromo());
            request.setPakaiMPay(transaksi.isPakaiMpay());


            String namaLengkap = String.format("%s %s", loginUser.getNamaDepan(), loginUser.getNamaBelakang());
            request.setNamaPelanggan(namaLengkap);
            request.setTelepon(loginUser.getNoTelepon());
            request.setType(ORDER);

            request.namaBarang = param.namaBarang;
            request.namaPengirim = param.namaPengirim;
            request.teleponPengirim = param.teleponPengirim;
            request.namaPenerima = param.namaPenerima;
            request.teleponPenerima = param.teleponPenerima;

        }
    }

    private void fcmBroadcast(int index) {
        Driver driverToSend = driverList.get(index);
        currentLoop++;
        request.setTime_accept(new Date().getTime()+"");
        FCMMessage message = new FCMMessage();
        message.setTo(driverToSend.getRegId());
        message.setData(request);

//        Log.e("REQUEST TO DRIVER", message.getData().toString());
        driver = driverToSend;

        FCMHelper.sendMessage(FCM_KEY, message).enqueue(new okhttp3.Callback() {
            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {

            }

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }


    @SuppressWarnings("unused")
    @Subscribe
    public void onMessageEvent(final DriverResponse response) {
        Log.e("DRIVER RESPONSE (W)", response.getResponse()+" "+response.getId()+" "+response.getIdTransaksi());
//        if (currentLoop < driverList.size()) {
            if (response.getResponse().equalsIgnoreCase(DriverResponse.ACCEPT)) {
                Log.d("DRIVER RESPONSE", "Terima");
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        threadRun = false;

                        for(Driver cDriver : driverList){
                            if(cDriver.getId().equals(response.getId())){
                                driver = cDriver;
                            }
                        }
//                        saveTransaction(transaksi);
//                        saveDriver(driver);
//                        Toast.makeText(getApplicationContext(), "Transaksi " + response.getIdTransaksi() + " ada yang mau!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(activity, InProgressActivity.class);
                        intent.putExtra("driver", driver);
                        intent.putExtra("request", request);
                        intent.putExtra("time_distance", timeDistance);
                        startActivity(intent);
                        finish();
                    }
                });
            }
//            else {
//                Log.d("DRIVER RESPONSE", "Tolak");
//                if(currentLoop == (driverList.size()-1)){
//                    Intent intent = new Intent(activity, InProgressActivity.class);
//                    intent.putExtra("driver", driver);
//                    intent.putExtra("request", request);
//                    intent.putExtra("time_distance", timeDistance);
//                    threadRun = false;
//                    startActivity(intent);
//                    finish();
//                }else{
////                    fcmBroadcast(++currentLoop);
//                    currentLoop++;
//                }
//
//            }
//        }
    }

    private void saveTransaction(Transaksi transaksi) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insert(transaksi);
        realm.commitTransaction();
    }

    private void saveDriver(Driver driver) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(Driver.class);
        realm.insert(driver);
        realm.commitTransaction();
    }
    private void updateLineDestination(String json) {
        Directions directions = new Directions(SendWaitingActivity.this);
        try {
            List<Route> routes = directions.parse(json);
            if (directionLine != null) directionLine.remove();
            if (routes.size() > 0) {
                directionLine = gMap.addPolyline((new PolylineOptions())
                        .addAll(routes.get(0).getOverviewPolyLine())
                        .color(ContextCompat.getColor(SendWaitingActivity.this, R.color.colorPrimaryDark))
                        .width(5));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.clear();
        requestRoute();
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(pickUpLatLang.latitude, pickUpLatLang.longitude), 15f));
        pickUpMarker = gMap.addMarker(new MarkerOptions()
                .position(pickUpLatLang)
                .title("Pick Up")
                .icon(BitmapDescriptorFactory.fromBitmap(iconSize((BitmapDrawable)getResources().getDrawable(R.mipmap.ic_lens)))));
        destinationMarker = gMap.addMarker(new MarkerOptions()
                .position(destinationLatLang)
                .title("Destination")
                .icon(BitmapDescriptorFactory.fromBitmap(iconSize((BitmapDrawable)getResources().getDrawable(R.drawable.lokasi_tujuan)))));
    }

    private Bitmap iconSize(BitmapDrawable bitmapdraw){
        int height = 45;
        int width = 45;
        Bitmap b=bitmapdraw.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }
}
