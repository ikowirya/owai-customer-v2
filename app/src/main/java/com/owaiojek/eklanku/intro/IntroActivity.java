package com.owaiojek.eklanku.intro;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.owaiojek.eklanku.home.LandingActivity;
import com.owaiojek.eklanku.splash.SplashActivity;

public class IntroActivity extends AppIntro2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceHelper preferenceHelper = new PreferenceHelper(getApplicationContext());
        if (preferenceHelper.isFirstTimeInstall()) {
            Fragment fragment1 = new Slide1();
            Fragment fragment2 = new Slide2();
            Fragment fragment3 = new Slide1();
            Fragment fragment4 = new Slide2();

            addSlide(fragment1);
            addSlide(fragment2);
            addSlide(fragment3);
            addSlide(fragment4);

            setBarColor(Color.parseColor("#3F51B5"));
            //setSeparatorColor(Color.parseColor("#2196F3"));

            // Hide Skip/Done button.
            showSkipButton(false);
            setProgressButtonEnabled(true);

            // Turn vibration on and set intensity.
            // NOTE: you will probably need to ask VIBRATE permission in Manifest.
            setVibrate(true);
            setVibrateIntensity(30);
            preferenceHelper.setFirstTimeInstall(false);
        }else {
            startActivity(new Intent(getApplicationContext(), SplashActivity.class));
            finish();
        }

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        startActivity(new Intent(getApplicationContext(), SplashActivity.class));
        finish();

    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }
}
