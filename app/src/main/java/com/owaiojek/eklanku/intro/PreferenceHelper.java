package com.owaiojek.eklanku.intro;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {
    private Context context;
    private static final String PREF_NAME = "myPref";
    private static final String IS_FIRST_TIME_INSTALL = "isFirstTimeInstall";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public PreferenceHelper(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setFirstTimeInstall(boolean isFirstTimeInstall){
        editor.putBoolean(IS_FIRST_TIME_INSTALL, isFirstTimeInstall);
        editor.commit();
    }

    public boolean isFirstTimeInstall(){
        return sharedPreferences.getBoolean(IS_FIRST_TIME_INSTALL,true);
    }
}
