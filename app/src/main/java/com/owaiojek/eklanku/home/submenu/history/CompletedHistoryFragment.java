package com.owaiojek.eklanku.home.submenu.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.adapter.HistoryAdapter;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.model.ItemHistory;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.menu.HistoryRequestJson;
import com.owaiojek.eklanku.model.json.menu.HistoryResponseJson;
import com.owaiojek.eklanku.utils.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CompletedHistoryFragment extends Fragment implements HistoryFragment.OnSwipeRefresh {

    @BindView(R.id.completed_recyclerView)
    RecyclerView recyclerView;
    HistoryAdapter historyAdapter;

    public CompletedHistoryFragment() {
    }

    public static CompletedHistoryFragment newInstance() {
        CompletedHistoryFragment fragment = new CompletedHistoryFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_completed_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        requestData();

    }

    private void requestData(){
        User user = MangJekApplication.getInstance(getActivity()).getLoginUser();
        HistoryRequestJson request = new HistoryRequestJson();
        request.id = user.getId();

        UserService service = ServiceGenerator.createService(UserService.class, user.getEmail(), user.getPassword());
        service.getCompleteHistory(request).enqueue(new Callback<HistoryResponseJson>() {
            @Override
            public void onResponse(Call<HistoryResponseJson> call, Response<HistoryResponseJson> response) {
                if (response.isSuccessful()) {
                    ArrayList<ItemHistory> data = response.body().data;

//                    Log.e("HISTORY", data.get(0).toString());

                    for(int i = 0;i<data.size();i++){
                        switch (data.get(i).order_fitur){
                            case "Bee-ride":
                                data.get(i).image_id = R.drawable.ic_mride;
                                break;
                            case "Bee-Car":
                                data.get(i).image_id = R.drawable.ic_mcar;
                                break;
                            case "Bee-send":
                                data.get(i).image_id = R.drawable.ic_msend;
                                break;
                            case "Bee-mart":
                                data.get(i).image_id = R.drawable.ic_mmart;
                                break;
                            case "Bee-box":
                                data.get(i).image_id = R.drawable.ic_mbox;
                                break;
                            case "Bee-service":
                                data.get(i).image_id = R.drawable.ic_mservice;
                                break;
                            case "Bee-massage":
                                data.get(i).image_id = R.drawable.ic_mmassage;
                                break;
                            case "Bee-food":
                                data.get(i).image_id = R.drawable.ic_mfood;
                                break;

                            default:
                                data.get(i).image_id = R.drawable.ic_mride;
                                break;
                        }
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

                    recyclerView.setLayoutManager(layoutManager);
                    historyAdapter = new HistoryAdapter(getContext(), data, true);
                    recyclerView.setAdapter(historyAdapter);
                    if (response.body().data.size() == 0) {
                        Log.d("HISTORY", "Empty");
                    }
                }
            }

            @Override
            public void onFailure(Call<HistoryResponseJson> call, Throwable t) {
                t.printStackTrace();
//                Toast.makeText(getActivity(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Log.e("System error:", t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        requestData();
    }

    @Override
    public void onRefresh() {
        requestData();
    }
}
