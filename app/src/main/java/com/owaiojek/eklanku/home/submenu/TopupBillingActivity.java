package com.owaiojek.eklanku.home.submenu;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.owaiojek.eklanku.R;

public class TopupBillingActivity extends AppCompatActivity implements View.OnClickListener {
    String bank, nominal, msg, logo, kode_bank, norek, namarek;
    TextView _tvnorek, _tvnamarek, _tvkodebank, _tvnamabank, _tvNominal, _tvtanggal, _keterangan;
    ImageView _imgbank, _btn_copy_total, _btn_copy_rekening;
    TextView txtUserid, txtNominal, txtBank, txtTime, txtKeterangan, tvWaktu;
    EditText edMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup_billing);
        initView();
        bindFromExtras();

    }

    public void bindFromExtras(){
        bank = getIntent().getExtras().getString("bank", "");
        nominal = getIntent().getExtras().getString("nominal", "");
        msg = getIntent().getExtras().getString("pesan", "");
        logo = getIntent().getExtras().getString("logo", "");
        kode_bank = getIntent().getExtras().getString("kode_bank", "");
        norek = getIntent().getExtras().getString("norek", "");
        namarek = getIntent().getExtras().getString("namarek", "");

        _tvnamabank.setText(bank);
        _tvkodebank.setText(kode_bank);
        _tvNominal.setText(nominal);
        _tvnamabank.setText(bank);
        _tvnorek.setText(norek);
        _tvnamarek.setText(namarek);
        edMessage.setText(msg);
        Glide.with(this).load(logo)
                .thumbnail(0.5f)
                .into(_imgbank);
    }

    public void initView(){
        txtUserid = findViewById(R.id.txt_user_id);
        txtNominal = findViewById(R.id.txt_nominal);
        txtBank = findViewById(R.id.txt_bank);
        txtTime = findViewById(R.id.txt_time);
        edMessage = findViewById(R.id.txt_message);
        txtKeterangan = findViewById(R.id.textKetKonfirmasiTopupBill);
        _tvkodebank = findViewById(R.id.tvkodebank);
        _tvnamabank = findViewById(R.id.tvnamabank);
        _tvnamarek = findViewById(R.id.tvnamarek);
        _imgbank = findViewById(R.id.imgbankBilling);
        _tvnorek = findViewById(R.id.tvnorek);
        _tvNominal = findViewById(R.id.tvNominal);

        _btn_copy_total = findViewById(R.id.btn_copy_total);
        _btn_copy_rekening = findViewById(R.id.btn_copy_rekening);
        _btn_copy_total.setOnClickListener(this);
        _btn_copy_rekening.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_copy_rekening:
                copyToClipboard(_tvnorek,"Salin rekening tujuan");
                break;
            case R.id.btn_copy_total:
                copyToClipboard(_tvNominal,"Salin jumlah");
                break;
        }
    }

    public void copyToClipboard(TextView textView,String info){
        ClipboardManager clipboard = (ClipboardManager) TopupBillingActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
        String value = textView.getText().toString();
        value = value.replace("-", "");
        ClipData clip = ClipData.newPlainText(info, value);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(TopupBillingActivity.this, info, Toast.LENGTH_SHORT).show();
    }


}
