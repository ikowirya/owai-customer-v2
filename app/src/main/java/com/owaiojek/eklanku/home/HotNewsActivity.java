package com.owaiojek.eklanku.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.adapter.NewsAdapter;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.model.NewsOtu;
import com.owaiojek.eklanku.model.NewsOtuResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;

public class HotNewsActivity extends AppCompatActivity {

    @BindView(R.id.imgNews)
    ImageView imgNews;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtContent)
    TextView txtContent;
    @BindView(R.id.txtAuthor)
    TextView txtAuthor;
    @BindView(R.id.txtDate)
    TextView txtDate;
    OtuService otuService;
    String berita_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_news);
        ButterKnife.bind(this);
//        webView.getSettings().setLoadsImagesAutomatically(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setDomStorageEnabled(true);
//
//        // Tiga baris di bawah ini agar laman yang dimuat dapat
//        // melakukan zoom.
//        webView.getSettings().setSupportZoom(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDisplayZoomControls(false);
//        // Baris di bawah untuk menambahkan scrollbar di dalam WebView-nya
//        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        webView.setWebViewClient(new WebViewClient());
//        webView.loadUrl("https://www.eklankumax.com/Informasi#section-1");
        Intent intent = getIntent();
        berita_id = intent.getStringExtra("berita_id");

        otuService = OtuApi.getClient().create(OtuService.class);
        getDetailNews();
    }

    private void getDetailNews() {
        retrofit2.Call<NewsOtu> users = otuService.requestNewsDetail(berita_id);
        users.enqueue(new Callback<NewsOtu>() {
            @Override
            public void onResponse(retrofit2.Call<NewsOtu> call, retrofit2.Response<NewsOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS")){
                        String url = response.body().getData().get(0).getBerita_gambar();
                        Glide.with(HotNewsActivity.this)
                                .load(url)
                                .thumbnail(0.5f)
                                .into(imgNews);
                        txtTitle.setText(response.body().getData().get(0).getBerita_judul());
                        txtContent.setText(response.body().getData().get(0).getBerita_detail());
                        txtAuthor.setText(response.body().getData().get(0).getBerita_author());
                        txtDate.setText(response.body().getData().get(0).getBerita_date());
                    }

                    else {
                        finish();
                        Toast.makeText(getApplicationContext(), "Connetion Problem", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<NewsOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
