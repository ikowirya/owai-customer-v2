package com.owaiojek.eklanku.home.submenu;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;

import com.bumptech.glide.Glide;
import com.owaiojek.eklanku.adapter.BankAdapter;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.home.MainActivity;
import com.owaiojek.eklanku.model.BankListOtu;
import com.owaiojek.eklanku.model.BankOtu;
import com.owaiojek.eklanku.model.ResponseTopUp;
import com.owaiojek.eklanku.model.json.user.SendOtuResponseJson;
import com.owaiojek.eklanku.model.json.user.SendTokenJson;
import com.owaiojek.eklanku.model.json.user.UpdateSaldoOwaiOtuJson;
import com.owaiojek.eklanku.signIn.SignInActivity;
import com.owaiojek.eklanku.utils.Log;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.user.TopupRequestJson;
import com.owaiojek.eklanku.model.json.user.TopupResponseJson;
import com.owaiojek.eklanku.utils.Utility;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class TopUpActivity extends AppCompatActivity {

    private static final String TAG = TopUpActivity.class.getSimpleName();

    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;
    String bukti;
    private String bankName = "";
    TopUpActivity activity;

//    @BindView(R.id.pemilikRekening)
//    EditText name;
//    @BindView(R.id.nomorRekening)
//    EditText accountNumber;
    @BindView(R.id.nominalTransfer)
    EditText nominal;
    @BindView(R.id.spinBank)
    Spinner spinner;
//    @BindView(R.id.butUploadBukti)
//    TextView upload;
    @BindView(R.id.butTopup)
    TextView topup;
//    @BindView(R.id.other_bank_layout)
//    TextInputLayout otherBankLayout;
//    @BindView(R.id.other_bank)
//    EditText otherBank;
    @BindView(R.id.rvData)
    RecyclerView rvData;

    OtuService otuService;
    RecyclerView.LayoutManager mLayoutManager;
    BankAdapter bankAdapter;
    String valueJumlah;
    User userLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);
        ButterKnife.bind(this);
        activity = TopUpActivity.this;
        otuService = OtuApi.getClient().create(OtuService.class);
        mLayoutManager = new LinearLayoutManager(this);
        rvData.setLayoutManager(mLayoutManager);
        final SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref",0);
        final String userID = preferences.getString("userID", "");
        final String mbr_token = preferences.getString("mbr_token", "");
        getBank(userID,mbr_token,"OWAICUSTOMER");
        userLogin = MangJekApplication.getInstance(this).getLoginUser();

//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            upload.setEnabled(false);
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
//        }

//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i != 3) {
//                    bankName = spinner.getSelectedItem().toString();
//                    otherBankLayout.setVisibility(GONE);
//                } else {
//                    otherBankLayout.setVisibility(VISIBLE);
//                    otherBank.requestFocus();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

//        otherBank.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                bankName = otherBank.getText().toString();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        nominal.addTextChangedListener(Utility.currencyTW(nominal));

//        upload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                takePhoto();
//            }
//        });

        topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                submitTopUp();
                 valueJumlah =  nominal.getText().toString();
                if (!valueJumlah.isEmpty()||!bankName.isEmpty()){
                    topupOtu(userID, mbr_token, "OWAICUSTOMER", bankName,valueJumlah);
                }else {
                    Snackbar.make(findViewById(R.id.main), "Please, Completed Form Data!", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

    }
    private void topupOtu(String userID, String accessToken, String aplUse, final String bank, String nominal) {
        retrofit2.Call<ResponseTopUp> users = otuService.requestBank(userID,accessToken,aplUse,bank,nominal);
        users.enqueue(new Callback<ResponseTopUp>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseTopUp> call, retrofit2.Response<ResponseTopUp> response) {
                if (response.isSuccessful()) {
//                    if (response.body().getStatus().equals("SUCCESS"))
//                    {
//                        valueJumlah = response.body().getNominal();
//                        updateSaldoOwaiOtu();
//                    }
//                    else {
//                        //Snackbar.make(findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), ""+ response.body().getRespMessage(), Toast.LENGTH_SHORT).show();
//                    }
                    String status = response.body().getStatus();
                    String error = response.body().getRespMessage();
                    //Log.d("OPPO-1", "onResponse>>>>>>>>>>>>>: " + error);

                    if (status.equals("SUCCESS")) {
                        Intent intent = new Intent(getBaseContext(), TopupBillingActivity.class);
                        intent.putExtra("bank", bank);
                        intent.putExtra("nominal", response.body().getNominal());
                        intent.putExtra("pesan", error);
                        intent.putExtra("kode_bank", response.body().getKode_bank());
                        intent.putExtra("logo", response.body().getLogo());
                        intent.putExtra("norek", response.body().getNomer_rekening());
                        intent.putExtra("namarek", response.body().getNama_pemilik());
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseTopUp> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateSaldoOwaiOtu(){

        UpdateSaldoOwaiOtuJson request = new UpdateSaldoOwaiOtuJson();
        request.nominal = valueJumlah;
        request.id_customer = userLogin.getId();

        UserService service = ServiceGenerator.createService(UserService.class, userLogin.getEmail(), userLogin.getPassword());
        service.updateSaldoOtu(request).enqueue(new Callback<SendOtuResponseJson>() {
            @Override
            public void onResponse(Call<SendOtuResponseJson> call, Response<SendOtuResponseJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("success")) {
                        //Snackbar.make(findViewById(R.id.main), "TopUp Successful", Snackbar.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Topup Successful", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SendOtuResponseJson> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBank(final String userID, final String accessToken, String aplUse ){
        retrofit2.Call<BankOtu> users = otuService.getBank(userID,accessToken,aplUse);
        users.enqueue(new Callback<BankOtu>() {
            @Override
            public void onResponse(retrofit2.Call<BankOtu> call, retrofit2.Response<BankOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {

                        List<String> list = new ArrayList<String>(); // List of Items
                        List<BankListOtu> bankListOtus= response.body().getBanklist();
                        for (int i = 0; i < bankListOtus.size(); i++) {
                            if (bankListOtus.size() > 0){
                                list.add(bankListOtus.get(i).getBank());
                            }
                        }
                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                bankName =  adapterView.getItemAtPosition(i).toString();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>
                                (TopUpActivity.this, android.R.layout.simple_spinner_item, list){
                            //By using this method we will define how
                            // the text appears before clicking a spinner
                            public View getView(int position, View convertView,
                                                ViewGroup parent) {
                                View v = super.getView(position, convertView, parent);
//                                ((TextView) v).setTextColor(Color.parseColor("#f7b914"));
                                return v;
                            }
                            //By using this method we will define
                            //how the listview appears after clicking a spinner
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View v = super.getDropDownView(position, convertView,
                                        parent);
                                v.setBackgroundColor(Color.parseColor("#f7b914"));
                                ((TextView) v).setTextColor(Color.parseColor("#ffffff"));
                                return v;
                            }
                        };
                        SpinnerAdapter.setDropDownViewResource(
                                android.R.layout.simple_spinner_dropdown_item);
                        // Set Adapter in the spinner
                        spinner.setAdapter(SpinnerAdapter);
                        bankAdapter = new BankAdapter(bankListOtus, TopUpActivity.this, new BankAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(BankListOtu item) {
                                if (!nominal.getText().toString().isEmpty()){
                                    ShowDialog(userID,accessToken,item,nominal.getText().toString());
                                }else {
                                    Toast.makeText(TopUpActivity.this, "Please complete form..", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        rvData.setAdapter(bankAdapter);

                    }

                    else {
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<BankOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void submitTopUp() {
        final ProgressDialog pd = showLoading();

        User user = MangJekApplication.getInstance(this).getLoginUser();
        TopupRequestJson request = new TopupRequestJson();
        request.id = user.getId();
//        request.atas_nama = name.getText().toString();
//        request.no_rekening = accountNumber.getText().toString();
        request.jumlah = getNominal();
        request.bank = bankName;
        request.bukti = bukti;


        UserService service = ServiceGenerator.createService(UserService.class, user.getEmail(), user.getPassword());
        service.topUp(request).enqueue(new Callback<TopupResponseJson>() {
            @Override
            public void onResponse(Call<TopupResponseJson> call, Response<TopupResponseJson> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();

                    if (response.body().message.equals("success")) {
                        Toast.makeText(activity, "Terima kasih. Verifikasi akan segera diproses..", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(activity, "Verifikasi bermasalah..", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<TopupResponseJson> call, Throwable t) {
                t.printStackTrace();
                pd.dismiss();
                Toast.makeText(TopUpActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    @OnClick(R.id.img_back_topup)
    void onBackClick(){
        finish();
    }

    private String getNominal() {
        String originalString = nominal.getText().toString();

        Long longval;
        if (originalString.contains(".")) {
            originalString = originalString.replaceAll("[$.]", "");
        }
        if (originalString.contains(",")) {
            originalString = originalString.replaceAll(",", "");
        }
        if (originalString.contains("Rp ")) {
            originalString = originalString.replaceAll("Rp ", "");
        }
        if (originalString.contains("Rp")) {
            originalString = originalString.replaceAll("Rp", "");
        }
        if (originalString.contains("R")) {
            originalString = originalString.replaceAll("R", "");
        }
        if (originalString.contains("p")) {
            originalString = originalString.replaceAll("p", "");
        }
        if (originalString.contains(" ")) {
            originalString = originalString.replaceAll(" ", "");
        }

        return originalString;
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        if (requestCode == 0) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//                upload.setEnabled(true);
//            }
//        }
//    }


    public void takePhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, TAKE_PICTURE);
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case TAKE_PICTURE:
//                if (resultCode == Activity.RESULT_OK) {
//                    Uri selectedImage = data.getData();
//                    try {
//                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
//                        Log.d("after_comppres", String.valueOf(bitmap.getByteCount()));
//                        bukti = compressJSON(bitmap);
//                        if (!bukti.equals("")) {
//                            ImageView centang = (ImageView) activity.findViewById(R.id.centang);
//                            centang.setVisibility(VISIBLE);
//
//                        }
//
//                    } catch (Exception e) {
//                        Toast.makeText(activity, "Failed to load", Toast.LENGTH_SHORT).show();
//                        Log.e("Camera", e.toString());
//                    }
//                }
//                break;
//            default:
//                break;
//        }
//    }


    private ProgressDialog showLoading() {
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }

//    public String compressJSON(Bitmap bmp) {
//        byte[] imageBytes0;
//        ByteArrayOutputStream baos0 = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos0);
//        imageBytes0 = baos0.toByteArray();
//        String encodedImage = Base64.encodeToString(imageBytes0, Base64.DEFAULT);
//        return encodedImage;
//    }

    public void ShowDialog(final String userID, final String accessToken, final BankListOtu item, String nominal){
        TextView tvDeposit,tvMethod;
        Button btnContinue,btnCancelTopup;
        ImageView ivLogo;

        final Dialog dialog = new Dialog(TopUpActivity.this);

        View view = View.inflate(TopUpActivity.this, R.layout.dialog_topup_saldo,null);
        tvDeposit = view.findViewById(R.id.tvDepositDialogTopup);
        tvMethod = view.findViewById(R.id.tvMethodDialogTopup);
        btnCancelTopup = view.findViewById(R.id.btnCancelTopup);
        btnContinue = view.findViewById(R.id.btnContinueTopup);
        ivLogo = view.findViewById(R.id.ivLogoBankTopup);

        tvDeposit.setText(nominal);
        tvMethod.setText(item.getBank());
        Glide.with(TopUpActivity.this).load(item.getLogo())
                .thumbnail(0.5f)
                .into(ivLogo);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topupOtu(userID, accessToken,"OWAICUSTOMER", item.getBank(), getNominal());
            }
        });

        btnCancelTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();

    }

}
