package com.owaiojek.eklanku.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.home.submenu.ViewPagerAdapter;
import com.owaiojek.eklanku.home.submenu.help.HelpFragment;
import com.owaiojek.eklanku.home.submenu.history.HistoryFragment;
import com.owaiojek.eklanku.home.submenu.home.HomeFragment;
import com.owaiojek.eklanku.home.submenu.setting.ChangePasswordActivity;
import com.owaiojek.eklanku.home.submenu.setting.FAQActivity;
import com.owaiojek.eklanku.home.submenu.setting.PrivacyPolicyActivity;
import com.owaiojek.eklanku.home.submenu.setting.SettingFragment;
import com.owaiojek.eklanku.home.submenu.setting.TermOfServiceActivity;
import com.owaiojek.eklanku.home.submenu.setting.UpdateProfileActivity;
import com.owaiojek.eklanku.mRideCar.WaitingActivity;
import com.owaiojek.eklanku.model.DiskonMpay;
import com.owaiojek.eklanku.model.Fitur;
import com.owaiojek.eklanku.model.MfoodMitra;
import com.owaiojek.eklanku.model.ResponseRegisterOtu;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.user.GetFiturResponseJson;
import com.owaiojek.eklanku.model.json.user.GetProfileRequestJson;
import com.owaiojek.eklanku.model.json.user.UpdateProfileRequestJson;
import com.owaiojek.eklanku.model.json.user.UpdateProfileResponseJson;
import com.owaiojek.eklanku.splash.SplashActivity;
import com.owaiojek.eklanku.utils.DialogActivity;
import com.owaiojek.eklanku.utils.MangJekTabProvider;
import com.owaiojek.eklanku.utils.MenuSelector;
import com.owaiojek.eklanku.utils.SnackbarController;
import com.owaiojek.eklanku.utils.view.CustomViewPager;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bradhawk on 10/10/2016.
 */

public class MainActivity extends DialogActivity implements SnackbarController,DrawerLayout.DrawerListener {

    @BindView(R.id.main_container)
    LinearLayout mainLayout;
    //RelativeLayout mainLayout;
//    @BindView(R.id.main_tabLayout)
//    SmartTabLayout mainTabLayout;
    @BindView(R.id.bottomMenu)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.main_viewPager)
    CustomViewPager mainViewPager;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar_main)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvEmaiId)
    TextView tvEmailId;
    @BindView(R.id.tvNumber)
    TextView tvPhoneNumber;
    @BindView(R.id.img_profile)
    CircleImageView img_profile;
    @BindView(R.id.img_profile_nav)
    CircleImageView img_profile_nav;

    private byte[] bytes;

    private static final int TAKE_PICTURE = 1;

    private Snackbar snackBar;
    private ActionBarDrawerToggle toggle;
    private MenuSelector selector;
    private SmartTabLayout.TabProvider tabProvider;
    private User loginUser;
    private FragmentPagerItemAdapter adapter;
    String userID,mbr_token,token;
    SharedPreferences preferences;
    boolean doubleBackToExitPressedOnce = false;
    OtuService otuService;

    MenuItem prevMenuItem;

    User user;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        user = MangJekApplication.getInstance(this).getLoginUser();
        otuService = OtuApi.getClient().create(OtuService.class);
        preferences = getApplicationContext().getSharedPreferences("MyPref",0);
        userID = preferences.getString("userID", "");
        mbr_token = preferences.getString("mbr_token", "");
        token = preferences.getString("token", "");
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        getPhoto();
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {

                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        img_profile_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();
            }
        });


        //setupTabLayoutViewPager();
        setupBottomNavigationView();
        setupViewPager(mainViewPager);

        //printKeyHash(this);
    }

    private void getPhoto(){
        UpdateProfileRequestJson request = new UpdateProfileRequestJson();
        request.email = user.getEmail();

        UserService service = ServiceGenerator.createService(UserService.class, user.getEmail(), user.getPassword());
        service.getProfile(request).enqueue(new Callback<GetProfileRequestJson>() {
            @Override
            public void onResponse(Call<GetProfileRequestJson> call, Response<GetProfileRequestJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().message.equals("success")) {
                        //update_data();
                        String url = "http://oway.co.id/admin/uploads/fotocustomer/"+response.body().data.get(0).foto_profile;
                        Glide.with(MainActivity.this)
                                .load(url)
                                .thumbnail(0.5f)
                                .into(img_profile);
                        Glide.with(MainActivity.this)
                                .load(url)
                                .thumbnail(0.5f)
                                .into(img_profile_nav);
//                        Toast.makeText(MainActivity.this, ""+response.body().data.get(0).foto_profile, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetProfileRequestJson> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(MainActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void selectPicture() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, TAKE_PICTURE);
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.d("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.d("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    try {
                        InputStream imageStream = getApplicationContext().getContentResolver().openInputStream(selectedImage);
                        bytes = convertBitmapToByteArray(BitmapFactory.decodeStream(imageStream));
                        String hasilFoto = "data:image/jpeg;base64,"+Base64.encodeToString(bytes, Base64.DEFAULT);
                        uploadFoto(hasilFoto);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Failed to load", Toast.LENGTH_SHORT).show();
                        Log.e("Camera", e.toString());
                    }
                }
                break;
            default:
                break;
        }
    }

    private void uploadFoto(final String hasilFoto) {
        showProgressDialog(R.string.dialog_loading);
        UpdateProfileRequestJson request = new UpdateProfileRequestJson();
        request.email = user.getEmail();
        request.nama_depan = user.getNamaDepan();
        request.nama_belakang = user.getNamaBelakang();
        request.no_telepon = user.getNoTelepon();
        request.tgl_lahir = user.getTglLahir();
        request.tempat_lahir = user.getTempatLahir();
        request.alamat = user.getAlamat();
        request.foto_profile = hasilFoto;

        UserService service = ServiceGenerator.createService(UserService.class, user.getEmail(), user.getPassword());
        service.updateProfile(request).enqueue(new Callback<UpdateProfileResponseJson>() {
            @Override
            public void onResponse(Call<UpdateProfileResponseJson> call, Response<UpdateProfileResponseJson> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().message.equals("success"))
                    {
                        getPhoto();

                        Realm realm = MangJekApplication.getInstance(MainActivity.this).getRealmInstance();
                        realm.beginTransaction();
                        MangJekApplication.getInstance(MainActivity.this).getLoginUser().setFoto_profile(hasilFoto);
                        realm.commitTransaction();
                        Intent intent = getIntent();
                        overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponseJson> call, Throwable t) {
                hideProgressDialog();
                t.printStackTrace();
                Toast.makeText(MainActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }

        });
    }


    private byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return baos.toByteArray();
    }

    public String compressJSON(Bitmap bmp) {
        byte[] imageBytes0;
        ByteArrayOutputStream baos0 = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos0);
        imageBytes0 = baos0.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes0, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Silahkan tekan tombol 'BACK' lagi untuk keluar...", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private void setupTabLayoutViewPager() {
//        tabProvider = new MangJekTabProvider(this);
//        selector = (MenuSelector) tabProvider;
//        mainTabLayout.setCustomTabView(tabProvider);
//
//        adapter = new FragmentPagerItemAdapter(
//                getSupportFragmentManager(), FragmentPagerItems.with(this)
//                .add(R.string.main_menuHome,    HomeFragment.class)
//                .add(R.string.main_menuHistory, HistoryFragment.class)
//                .add(R.string.main_menuHelp, HelpFragment.class)
//              //  .add(R.string.main_menuSetting, SettingFragment.class)
//                .create());
//        mainViewPager.setAdapter(adapter);
//        mainTabLayout.setViewPager(mainViewPager);
//        mainViewPager.setPagingEnabled(false);
//
//        mainTabLayout.setOnTabClickListener(new SmartTabLayout.OnTabClickListener() {
//            @Override
//            public void onTabClicked(int position) {
//                selector.selectMenu(position);
//            }
//        });
    }

    private void setupViewPager(ViewPager viewPager)

    {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        HomeFragment homeFragment = new HomeFragment();

        HistoryFragment historyFragment = new HistoryFragment();

        HelpFragment helpFragment =new HelpFragment();

        adapter.addFragment(homeFragment);

        adapter.addFragment(historyFragment);

        adapter.addFragment(helpFragment);

        viewPager.setAdapter(adapter);

    }

    private void setupBottomNavigationView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_home:
                        mainViewPager.setCurrentItem(0);
                        break;
                    case R.id.action_order:
                        mainViewPager.setCurrentItem(1);
                        break;
                    case R.id.action_help:
                        mainViewPager.setCurrentItem(2);
                        break;
                }

                return false;
            }
        });

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else
                {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: "+i);
                bottomNavigationView.getMenu().getItem(i).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginUser = MangJekApplication.getInstance(this).getLoginUser();
        tvName.setText(String.format("%s %s", loginUser.getNamaDepan(), loginUser.getNamaBelakang()));
        tvEmailId.setText(loginUser.getEmail());
        tvPhoneNumber.setText(loginUser.getNoTelepon());

        updateFiturMangJek();
    }

    @Override
    public void showSnackbar(@StringRes int stringRes, int duration, @StringRes int actionResText, View.OnClickListener onClickListener) {
        snackBar = Snackbar.make(mainLayout, stringRes, duration);
        if (actionResText != -1 && onClickListener != null) {
            snackBar.setAction(actionResText, onClickListener)
                    .setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        snackBar.show();
    }

    private void updateFiturMangJek() {
        User loginUser = MangJekApplication.getInstance(this).getLoginUser();
        UserService userService = ServiceGenerator.createService(UserService.class,
                loginUser.getEmail(), loginUser.getPassword());
        userService.getFitur().enqueue(new Callback<GetFiturResponseJson>() {
            @Override
            public void onResponse(Call<GetFiturResponseJson> call, Response<GetFiturResponseJson> response) {
                if (response.isSuccessful()) {
                    Realm realm = MangJekApplication.getInstance(MainActivity.this).getRealmInstance();
                    realm.beginTransaction();
                    realm.delete(Fitur.class);
                    realm.copyToRealm(response.body().getData());
                    realm.commitTransaction();

                    DiskonMpay diskonMpay = response.body().getDiskonMpay();
                    realm.beginTransaction();
                    realm.delete(DiskonMpay.class);
                    realm.copyToRealm(response.body().getDiskonMpay());
                    realm.commitTransaction();
                    MangJekApplication.getInstance(MainActivity.this).setDiskonMpay(diskonMpay);

                    MfoodMitra mfoodMitra = response.body().getMfoodMitra();
                    realm.beginTransaction();
                    realm.delete(MfoodMitra.class);
                    realm.copyToRealm(response.body().getMfoodMitra());
                    realm.commitTransaction();
                    MangJekApplication.getInstance(MainActivity.this).setMfoodMitra(mfoodMitra);
                }
            }

            @Override
            public void onFailure(Call<GetFiturResponseJson> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDrawerSlide(@NonNull View view, float v) {

    }

    @Override
    public void onDrawerOpened(@NonNull View view) {
        view.bringToFront();  //add this two lines
        view.requestLayout();
    }

    @Override
    public void onDrawerClosed(@NonNull View view) {

    }

    @Override
    public void onDrawerStateChanged(int i) {

    }

    @OnClick({R.id.nav_change_password,R.id.nav_reqiusite_certainty,R.id.nav_privacy_policy,R.id.nav_faq,
            R.id.nav_rate_aps,R.id.nav_exit})
    public void onNavItemClick(View view){
        switch (view.getId()){
            case R.id.nav_change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_reqiusite_certainty:
                startActivity(new Intent(this, TermOfServiceActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_privacy_policy:
                startActivity(new Intent(this, PrivacyPolicyActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_faq:
                startActivity(new Intent(this, FAQActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_rate_aps:
                final String appPackageName = this.getPackageName();
//                final String appPackageName = "net.gumcode.drivermangjek";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_exit:
                Realm realm = MangJekApplication.getInstance(MainActivity.this).getRealmInstance();
                realm.beginTransaction();
                realm.delete(User.class);
                realm.commitTransaction();
                startActivity(new Intent(MainActivity.this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                MangJekApplication.getInstance(MainActivity.this).setLoginUser(null);
                drawer.closeDrawer(GravityCompat.START);
                logout(userID,mbr_token,token,"OWAICUSTOMER");

                break;
        }
    }

    private void logout(String userID, String mbr_token, String token, String aplUse) {
        retrofit2.Call<ResponseRegisterOtu> users = otuService.logout(userID,mbr_token,token,aplUse);
        users.enqueue(new Callback<ResponseRegisterOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseRegisterOtu> call, retrofit2.Response<ResponseRegisterOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        Toast.makeText(getApplicationContext(), "Logout Successful", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Session End", Toast.LENGTH_SHORT).show();
                    }
                    preferences.edit().clear().commit();
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseRegisterOtu> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onUserProfileClick(){
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_right_burger:
                onUserProfileClick();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
