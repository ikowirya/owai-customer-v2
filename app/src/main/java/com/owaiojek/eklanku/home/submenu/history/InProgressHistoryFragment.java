package com.owaiojek.eklanku.home.submenu.history;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.adapter.HistoryAdapter;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.model.ItemHistory;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.menu.HistoryRequestJson;
import com.owaiojek.eklanku.model.json.menu.HistoryResponseJson;
import com.owaiojek.eklanku.splash.SplashActivity;
import com.owaiojek.eklanku.utils.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InProgressHistoryFragment extends Fragment implements HistoryFragment.OnSwipeRefresh {

    @BindView(R.id.inProgress_recyclerView)
    RecyclerView recyclerView;
    HistoryAdapter historyAdapter;
    User user;

    public InProgressHistoryFragment() {
    }

    public static InProgressHistoryFragment newInstance() {
        InProgressHistoryFragment fragment = new InProgressHistoryFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_in_progress_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if(MangJekApplication.getInstance(getActivity()).getLoginUser() != null){
            user = MangJekApplication.getInstance(getActivity()).getLoginUser();
        }else{
            startActivity(new Intent(getActivity(), SplashActivity.class));
            getActivity().finish();
        }
        requestData();
    }


    private void requestData(){
        if(MangJekApplication.getInstance(getActivity()).getLoginUser() != null){
            user = MangJekApplication.getInstance(getActivity()).getLoginUser();
        }
        HistoryRequestJson request = new HistoryRequestJson();
        request.id = user.getId();

        UserService service = ServiceGenerator.createService(UserService.class, user.getEmail(), user.getPassword());
        service.getOnProgressHistory(request).enqueue(new Callback<HistoryResponseJson>() {
            @Override
            public void onResponse(Call<HistoryResponseJson> call, Response<HistoryResponseJson> response) {
                if (response.isSuccessful()) {
                    ArrayList<ItemHistory> data = response.body().data;

//                    Log.e("HISTORY", data.get(0).toString());

                    for(int i = 0;i<data.size();i++){
                        switch (data.get(i).order_fitur){
                            case "M-ride":
                                data.get(i).image_id = R.drawable.ic_mride;
                                break;
                            case "M-car":
                                data.get(i).image_id = R.drawable.ic_mcar;
                                break;
                            case "M-send":
                                data.get(i).image_id = R.drawable.ic_msend;
                                break;
                            case "M-mart":
                                data.get(i).image_id = R.drawable.ic_mmart;
                                break;
                            case "M-box":
                                data.get(i).image_id = R.drawable.ic_mbox;
                                break;
                            case "M-service":
                                data.get(i).image_id = R.drawable.ic_mservice;
                                break;
                            case "M-massage":
                                data.get(i).image_id = R.drawable.ic_mmassage;
                                break;
                            case "M-food":
                                data.get(i).image_id = R.drawable.ic_mfood;
                                break;

                            default:
                                data.get(i).image_id = R.drawable.ic_mride;
                                break;
                        }
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                    historyAdapter = new HistoryAdapter(getContext(), data, false);
                    recyclerView.setAdapter(historyAdapter);

                    if (response.body().data.size() == 0) {
                        Log.d("HISTORY", "Empty");

                    }
                }
            }

            @Override
            public void onFailure(Call<HistoryResponseJson> call, Throwable t) {
                t.printStackTrace();
//                Toast.makeText(getActivity(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Log.e("System error:", t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        requestData();
    }

    @Override
    public void onRefresh() {
        requestData();
    }
}
