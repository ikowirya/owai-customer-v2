package com.owaiojek.eklanku.home.submenu.home;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.adapter.NewsAdapter;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.home.submenu.TopUpActivity;
import com.owaiojek.eklanku.home.submenu.setting.UpdateProfileActivity;
import com.owaiojek.eklanku.mBox.BoxActivity;
import com.owaiojek.eklanku.mFood.FoodActivity;
import com.owaiojek.eklanku.mMart.MartActivity;
import com.owaiojek.eklanku.mMassage.MassageActivity;
import com.owaiojek.eklanku.mRideCar.RideCarActivity;
import com.owaiojek.eklanku.mSend.SendActivity;
import com.owaiojek.eklanku.mService.mServiceActivity;
import com.owaiojek.eklanku.model.Banner;
import com.owaiojek.eklanku.model.Fitur;
import com.owaiojek.eklanku.model.NewsOtu;
import com.owaiojek.eklanku.model.NewsOtuResponse;
import com.owaiojek.eklanku.model.SaldoBonusOtu;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.news.News;
import com.owaiojek.eklanku.model.json.news.NewsResponse;
import com.owaiojek.eklanku.model.json.user.GetBannerResponseJson;
import com.owaiojek.eklanku.model.json.user.GetProfileRequestJson;
import com.owaiojek.eklanku.model.json.user.GetSaldoRequestJson;
import com.owaiojek.eklanku.model.json.user.GetSaldoResponseJson;
import com.owaiojek.eklanku.model.json.user.UpdateProfileRequestJson;
import com.owaiojek.eklanku.splash.SplashActivity;
import com.owaiojek.eklanku.utils.ConnectivityUtils;
import com.owaiojek.eklanku.utils.Log;
import com.owaiojek.eklanku.utils.SnackbarController;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.motion.MotionScene.TAG;

/**
 * Created by bradhawk on 10/10/2016.
 */

public class HomeFragment extends Fragment {

    @BindView(R.id.home_mCar)
    LinearLayout buttonMangCar;

    @BindView(R.id.home_mRide)
    LinearLayout buttonMangRide;

    @BindView(R.id.home_mSend)
    LinearLayout buttonMangSend;

//    @BindView(R.id.home_mBox)
//    RelativeLayout buttonMangBox;
//
//    @BindView(R.id.home_mMart)
//    RelativeLayout buttonMangMart;
//
//    @BindView(R.id.home_mMassage)
//    RelativeLayout buttonMangMassage;
//
    @BindView(R.id.home_mFood)
    LinearLayout buttonMangFood;

//    @BindView(R.id.home_mService)
//    RelativeLayout butonMangService;

    @BindView(R.id.home_mPayBalance)
    TextView mPayBalance;

    @BindView(R.id.txtBonus)
    TextView txtBonus;

    @BindView(R.id.home_topUpButton)
    LinearLayout topUpButton;

    @BindView(R.id.home_siteButton)
    LinearLayout home_siteButton;

//    @BindView(R.id.slide_viewPager)
//    AutoScrollViewPager slideViewPager;

//    @BindView(R.id.slide_viewPager_indicator)
//    CircleIndicator slideIndicator;

    @BindView(R.id.linLayMyAccount)
    LinearLayout linLayMyAccount;

//    @BindView(R.id.img_profile_home)
//    CircleImageView img_profile_home;
    @BindView(R.id.ibReload)
    ImageButton ibReload;
    @OnClick(R.id.ibReload)
    void ReloadSaldo(){
        Animation animation = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate);
        ibReload.setAnimation(animation);
        getSaldoBonus(userID, mbr_token,"OWAICUSTOMER");
    }

    @BindView(R.id.rvNews)
    RecyclerView rvNews;

//    @BindView(R.id.rvData)
//    RecyclerView rvData;

    RecyclerView.LayoutManager mLayoutManager;
    NewsAdapter newsAdapter;


    private SnackbarController snackbarController;

    private boolean connectionAvailable;
    private boolean isDataLoaded = false;

    private Realm realm;

    private int successfulCall;
    String mbr_token,userID;
    OtuService otuService;
    public ArrayList<Banner> banners = new ArrayList<>();
    User user;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SnackbarController) {
            snackbarController = (SnackbarController) context;
        }
    }

//    @OnClick(R.id.img_profile_home)
//    void onProfileImageClick(){
//        Activity activity = getActivity();
//        if (activity instanceof MainActivity){
//            ((MainActivity)activity).onUserProfileClick();
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        user = MangJekApplication.getInstance(getActivity()).getLoginUser();
        otuService = OtuApi.getClient().create(OtuService.class);
        mLayoutManager = new LinearLayoutManager(getActivity());
        //rvData.setLayoutManager(mLayoutManager);

        SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
        userID = preferences.getString("userID", "");
        mbr_token = preferences.getString("mbr_token", "");
        getPhoto();
        getNews();
        //getNewsUpdate();

        linLayMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), UpdateProfileActivity.class));
            }
        });
        buttonMangRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMangRideClick();
            }
        });
        buttonMangCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMangCarClick();
            }
        });
        buttonMangSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMangSendClick();
            }
        });
//        buttonMangBox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onMangBoxClick();
//            }
//        });
        connectionAvailable = false;
        topUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTopUpClick();
            }
        });
        home_siteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://oway.co.id"));
                    getActivity().startActivity(browserIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "No application can handle this request."
                            + " Please install a Web Browser",  Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        });
//        buttonMangMart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onMangMartClick();
//            }
//        });
////        butonMangService.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                onMServiceClick();
////            }
////        });
//        buttonMangMassage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onMMassageClick();
//            }
//        });
        buttonMangFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMangFoodClick();
            }
        });

        realm = MangJekApplication.getInstance(getActivity()).getRealmInstance();
        getImageBanner();


    }

    private void getNews(){
        retrofit2.Call<NewsOtu> users = otuService.requestNews();
        users.enqueue(new Callback<NewsOtu>() {
            @Override
            public void onResponse(retrofit2.Call<NewsOtu> call, retrofit2.Response<NewsOtu> response) {
                if (response.isSuccessful()) {

                    if (response.body().getStatus().equals("SUCCESS")){
                        List<NewsOtuResponse> newsOtuResponseList = response.body().getData();
                        newsAdapter = new NewsAdapter(newsOtuResponseList);
                        //rvData.setAdapter(newsAdapter);
                        rvNews.setAdapter(newsAdapter);
                        rvNews.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayout.HORIZONTAL,false));

                    }

                    else {

                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<NewsOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPhoto(){
        UpdateProfileRequestJson request = new UpdateProfileRequestJson();
        request.email = user.getEmail();

        UserService service = ServiceGenerator.createService(UserService.class, user.getEmail(), user.getPassword());
        service.getProfile(request).enqueue(new Callback<GetProfileRequestJson>() {
            @Override
            public void onResponse(Call<GetProfileRequestJson> call, Response<GetProfileRequestJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().message.equals("success")) {
                        //update_data();
                        String url = "http://oway.co.id/admin/uploads/fotocustomer/"+response.body().data.get(0).foto_profile;
//                        Glide.with(getActivity())
//                                .load(url)
//                                .thumbnail(0.5f)
//                                .into(img_profile_home);
                    } else {
                        Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetProfileRequestJson> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getImageBanner() {
        User loginUser = new User();
        if (MangJekApplication.getInstance(getActivity()).getLoginUser() != null) {
            loginUser = MangJekApplication.getInstance(getActivity()).getLoginUser();
        } else {
            startActivity(new Intent(getActivity(), SplashActivity.class));
            getActivity().finish();
        }

        UserService userService = ServiceGenerator.createService(UserService.class,
                loginUser.getEmail(), loginUser.getPassword());
        userService.getBanner().enqueue(new Callback<GetBannerResponseJson>() {
            @Override
            public void onResponse(Call<GetBannerResponseJson> call, Response<GetBannerResponseJson> response) {
                if (response.isSuccessful()) {
                    banners = response.body().data;
                    Log.e("Image", response.body().data.get(0).foto);
                    MyPagerAdapter pagerAdapter = new MyPagerAdapter(getFragmentManager(), banners);
//                    slideViewPager.setAdapter(pagerAdapter);
//                    slideIndicator.setViewPager(slideViewPager);
//                    slideViewPager.setInterval(20000);
//                    slideViewPager.startAutoScroll(20000);
                }
            }

            @Override
            public void onFailure(Call<GetBannerResponseJson> call, Throwable t) {

            }
        });
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 5;
        public ArrayList<Banner> banners = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fragmentManager, ArrayList<Banner> banners) {
            super(fragmentManager);
            this.banners = banners;
        }

        @Override
        public int getCount() {
            return banners.size();
        }

        @Override
        public Fragment getItem(int position) {
            return SlideFragment.newInstance(banners.get(position).id, banners.get(position).foto);
//            switch (position) {
//                case 0:
//                    return SlideFragment.newInstance(0, "Page # 1");
//                case 1:
//                    return SlideFragment.newInstance(1, "Page # 2");
//                case 2:
//                    return SlideFragment.newInstance(2, "Page # 3");
//                case 3:
//                    return SlideFragment.newInstance(3, "Page # 4");
//                case 4:
//                    return SlideFragment.newInstance(4, "Page # 5");
//
//
//                default:
//                    return null;
//            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        successfulCall = 0;
        connectionAvailable = ConnectivityUtils.isConnected(getActivity());
        if (!connectionAvailable) {
            if (snackbarController != null) snackbarController.showSnackbar(
                    R.string.text_noInternet, Snackbar.LENGTH_INDEFINITE, R.string.text_close,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            return;
                        }
                    });
        } else {
//            updateMPayBalance();

            getSaldoBonus(userID, mbr_token,"OWAICUSTOMER");
        }
    }

    private void onMangSendClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 5).findFirst();
        Intent intent = new Intent(getActivity(), SendActivity.class);
        intent.putExtra(SendActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onTopUpClick() {
        Intent intent = new Intent(getActivity(), TopUpActivity.class);
        startActivity(intent);
    }

    private void onMangRideClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 1).findFirst();
        Intent intent = new Intent(getActivity(), RideCarActivity.class);
        intent.putExtra(RideCarActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onMangCarClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 2).findFirst();
        Intent intent = new Intent(getActivity(), RideCarActivity.class);
        intent.putExtra(RideCarActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onMangMartClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 4).findFirst();
        Intent intent = new Intent(getActivity(), MartActivity.class);
        intent.putExtra(MartActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onMangBoxClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 7).findFirst();
        Intent intent = new Intent(getActivity(), BoxActivity.class);
        intent.putExtra(BoxActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onMServiceClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 8).findFirst();
        Intent intent = new Intent(getActivity(), mServiceActivity.class);
        intent.putExtra(mServiceActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onMMassageClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 6).findFirst();
        Intent intent = new Intent(getActivity(), MassageActivity.class);
        intent.putExtra(mServiceActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onMangFoodClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 3).findFirst();
        Intent intent = new Intent(getActivity(), FoodActivity.class);
        intent.putExtra(FoodActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }


    private void updateMPayBalance() {
        User loginUser = MangJekApplication.getInstance(getActivity()).getLoginUser();
        UserService userService = ServiceGenerator.createService(
                UserService.class, loginUser.getEmail(), loginUser.getPassword());

        GetSaldoRequestJson param = new GetSaldoRequestJson();
        param.setId(loginUser.getId());
        userService.getSaldo(param).enqueue(new Callback<GetSaldoResponseJson>() {
            @Override
            public void onResponse(Call<GetSaldoResponseJson> call, Response<GetSaldoResponseJson> response) {
                if (response.isSuccessful()) {
                    String formattedText = String.format(Locale.US, "Rp. %s ,-",
                            NumberFormat.getNumberInstance(Locale.US).format(response.body().getData()));
                    mPayBalance.setText(formattedText);
                    successfulCall++;

                    if(HomeFragment.this.getActivity() != null) {
                        Realm realm = MangJekApplication.getInstance(HomeFragment.this.getActivity()).getRealmInstance();
                        User loginUser = MangJekApplication.getInstance(HomeFragment.this.getActivity()).getLoginUser();
                        realm.beginTransaction();
                        loginUser.setmPaySaldo(response.body().getData());
                        realm.commitTransaction();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetSaldoResponseJson> call, Throwable t) {

            }
        });
    }

    private void getSaldoBonus(String userID, String accessToken, String aplUse){
        retrofit2.Call<SaldoBonusOtu> users = otuService.saldoBonus(userID,accessToken,aplUse);
        users.enqueue(new Callback<SaldoBonusOtu>() {
            @Override
            public void onResponse(retrofit2.Call<SaldoBonusOtu> call, retrofit2.Response<SaldoBonusOtu> response) {
                if (response.isSuccessful()) {

                    android.util.Log.d(TAG, "onResponse: "+response.body().getStatus());
                    if (response.body().getStatus().equals("SUCCESS")){
//                        int saldo = Integer.parseInt(String.valueOf(response.body().getBalance().get(0).getMbr_amount()));
                        double saldo = response.body().getBalance().get(0).getSisa_uang();
                        double bonus = response.body().getBalance().get(0).getBonus_member();
//                        android.util.Log.d(TAG, "onResponsef: "+String.valueOf(saldo));
                        String formattedText = String.format(Locale.US, "Rp. %s ,-",
                                NumberFormat.getNumberInstance(Locale.US).format(saldo));
                        String bonusOtu = String.format(Locale.US, "Rp. %s ,-",
                                NumberFormat.getNumberInstance(Locale.US).format(bonus));
                        mPayBalance.setText(formattedText);
                        txtBonus.setText(bonusOtu);
                        successfulCall++;
                        if(HomeFragment.this.getActivity() != null) {
                            Realm realm = MangJekApplication.getInstance(HomeFragment.this.getActivity()).getRealmInstance();
                            User loginUser = MangJekApplication.getInstance(HomeFragment.this.getActivity()).getLoginUser();
                            realm.beginTransaction();
                            loginUser.setmPaySaldo((long) saldo);
                            realm.commitTransaction();
                        }
                    }

                    else {

                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<SaldoBonusOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    public void getNewsUpdate(){
//        OtuService otuService = OtuApi.getClient().create(OtuService.class);
//        Call<NewsResponse> call = otuService.getNews();
//        call.enqueue(new Callback<NewsResponse>() {
//            @Override
//            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
//                if (response.isSuccessful()){
//                    NewsResponse newsResponse = response.body();
//                    List<News> newsList = newsResponse.getData();
//                    NewsAdapter newsAdapter = new NewsAdapter(newsList,getActivity());
//                    rvNews.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
//                    rvNews.setAdapter(newsAdapter);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<NewsResponse> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
//    }


}
