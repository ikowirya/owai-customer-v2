package com.owaiojek.eklanku.home;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.model.FirebaseToken;
import com.owaiojek.eklanku.model.json.user.AccountKitLoginRequest;
import com.owaiojek.eklanku.model.json.user.AccountKitLoginResponse;
import com.owaiojek.eklanku.model.json.user.ResponseCheckMemberExist;
import com.owaiojek.eklanku.signIn.SignInActivity;
import com.owaiojek.eklanku.signUp.SignUpActivity;
import com.owaiojek.eklanku.utils.Log;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LandingActivity extends AppCompatActivity {

    public static final String SUCCESS_LOGIN = "success";
    public static final String FAILED_LOGIN = "failed";
    public static final String USER_ALREADY_REGISTERED = "user already registered";
    public static final String USER_NEED_TO_REGISTER = "user need to register";

    private static final int FRAMEWORK_REQUEST_CODE = 1;
    UIManager uiManager;

    private int nextPermissionsRequestCode = 4000;
    private final Map<Integer, OnCompleteListener> permissionsListeners = new HashMap<>();

    private interface OnCompleteListener {
        void onComplete();
    }

    @OnClick(R.id.btnAccountKit2)
    void ActionAccountKit(){
        onLogin(LoginType.PHONE,"ID");
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != FRAMEWORK_REQUEST_CODE) {
            return;
        }

        final String toastMessage;
        final AccountKitLoginResult loginResult = AccountKit.loginResultWithIntent(data);
        if (loginResult == null || loginResult.wasCancelled()) {
            toastMessage = "Login Cancelled";
            startActivity(new Intent(getApplicationContext(),SignInActivity.class));
            finish();
        } else if (loginResult.getError() != null) {
            toastMessage = loginResult.getError().getErrorType().getMessage();
        } else {
            final AccessToken accessToken = loginResult.getAccessToken();
            final long tokenRefreshIntervalInSeconds = loginResult.getTokenRefreshIntervalInSeconds();
            if (accessToken != null) {
                checkAccountKit(accessToken);
            } else {
                toastMessage = "Unknown response type";
            }
        }

//        Toast.makeText(
//                this,
//                toastMessage,
//                Toast.LENGTH_LONG)
//                .show();
    }

    private void onLogin(final LoginType loginType,String countryCode) {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        //MyAdvancedUIManager myAdvancedUIManager = new MyAdvancedUIManager(loginType);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(loginType, AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setDefaultCountryCode("ID");

        // Skin is CLASSIC, CONTEMPORARY, or TRANSLUCENT
        uiManager = new SkinManager(
                SkinManager.Skin.TRANSLUCENT,
                ContextCompat.getColor(this, R.color.PrimaryDarkYellow),
                R.color.PrimaryYellow,
                SkinManager.Tint.WHITE, 0.1);

        configurationBuilder.setUIManager(uiManager);
        configurationBuilder.setReadPhoneStateEnabled(true);
        configurationBuilder.setDefaultCountryCode(countryCode);
        configurationBuilder.setReceiveSMS(true);
        final AccountKitConfiguration configuration = configurationBuilder.build();
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configuration);
        OnCompleteListener completeListener = new OnCompleteListener() {
            @Override
            public void onComplete() {
                startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
            }
        };

        switch (loginType) {
            case EMAIL:
                if (!isGooglePlayServicesAvailable()) {
                    final OnCompleteListener getAccountsCompleteListener = completeListener;
                    completeListener = new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.GET_ACCOUNTS,
                                    R.string.permissions_get_accounts_title,
                                    R.string.permissions_get_accounts_message,
                                    getAccountsCompleteListener);
                        }
                    };
                }
                break;
            case PHONE:
                if (configuration.isReceiveSMSEnabled() && !canReadSmsWithoutPermission()) {
                    final OnCompleteListener receiveSMSCompleteListener = completeListener;
                    completeListener = new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.RECEIVE_SMS,
                                    R.string.permissions_receive_sms_title,
                                    R.string.permissions_receive_sms_message,
                                    receiveSMSCompleteListener);
                        }
                    };
                }
                if (configuration.isReadPhoneStateEnabled() && !isGooglePlayServicesAvailable()) {
                    final OnCompleteListener readPhoneStateCompleteListener = completeListener;
                    completeListener = new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.READ_PHONE_STATE,
                                    R.string.permissions_read_phone_state_title,
                                    R.string.permissions_read_phone_state_message,
                                    readPhoneStateCompleteListener);
                        }
                    };
                }
                break;
        }
        completeListener.onComplete();
    }

    private boolean isGooglePlayServicesAvailable() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        return googlePlayServicesAvailable == ConnectionResult.SUCCESS;
    }

    private boolean canReadSmsWithoutPermission() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        if (googlePlayServicesAvailable == ConnectionResult.SUCCESS) {
            return true;
        }
        //TODO we should also check for Android O here t18761104

        return false;
    }

    private void requestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        checkRequestPermissions(permission, rationaleTitleResourceId, rationaleMessageResourceId, listener);
    }

    @TargetApi(23)
    private void checkRequestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (shouldShowRequestPermissionRationale(permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            requestPermissions(new String[] { permission }, requestCode);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            // ignore and clean up the listener
                            permissionsListeners.remove(requestCode);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            requestPermissions(new String[]{ permission }, requestCode);
        }
    }

    @TargetApi(23)
    @SuppressWarnings("unused")
    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           final @NonNull String permissions[],
                                           final @NonNull int[] grantResults) {
        final OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        }
    }

    public void checkAccountKit(final AccessToken accessToken){
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                AccountKitLoginRequest accountKitLoginRequest = new AccountKitLoginRequest();
                accountKitLoginRequest.setCustomer_phone("0"+account.getPhoneNumber().getPhoneNumber());
                accountKitLoginRequest.setToken(accessToken.getToken());
                //accountKitLoginRequest.setToken("AA"+account.getId()+""+ General.ACCOUNT_KIT_SECRET_KEY);
                //Toast.makeText(getApplicationContext(),"token="+accountKitLoginRequest.getToken()+" customer_phone"+accountKitLoginRequest.getCustomer_phone(),Toast.LENGTH_LONG).show();
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LandingActivity.this);
//                alertDialog.setMessage("token="+accountKitLoginRequest.getToken()+" customer_phone"+accountKitLoginRequest.getCustomer_phone());
//                alertDialog.show();
                loginToOway(accountKitLoginRequest);
            }

            @Override
            public void onError(AccountKitError accountKitError) {
                Log.e("AccountKitError",accountKitError.getErrorType().getMessage());
            }
        });
    }

    public void loginToOway(AccountKitLoginRequest param){
        UserService service = ServiceGenerator.createService2(UserService.class);
        Call<AccountKitLoginResponse> call = service.loginWithAccountKit(param);
        call.enqueue(new Callback<AccountKitLoginResponse>() {
            @Override
            public void onResponse(Call<AccountKitLoginResponse> call, Response<AccountKitLoginResponse> response) {
                if (response.isSuccessful()){
                    AccountKitLoginResponse res = response.body();
                    switch (res.getMessage()){
                        case USER_ALREADY_REGISTERED:
                            checkIsUserExistOtu(param.getCustomer_phone(),res.getData());
                            break;
                        case USER_NEED_TO_REGISTER:
                            checkIsUserExistOtu(param.getCustomer_phone(),null);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<AccountKitLoginResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void checkIsUserExistOtu(String UserId, String email){
        OtuService otuService = OtuApi.getClient().create(OtuService.class);
        Call<ResponseCheckMemberExist> call = otuService.getMember(UserId,"OWAICUSTOMER");
        call.enqueue(new Callback<ResponseCheckMemberExist>() {
            @Override
            public void onResponse(Call<ResponseCheckMemberExist> call, Response<ResponseCheckMemberExist> response) {
                if (response.isSuccessful()){
                    ResponseCheckMemberExist res = response.body();
                    if (res.getStatus().equals("SUCCESS")){
                        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.putExtra("UserId", UserId);
                        intent.putExtra("Email", email);
                        startActivity(intent);
                    }else {
                        if (res.getRespMessage().equals("YOUR PHONE NUMBER IS ALREADY REGISTERED IN OUR SYSTEM")){
                            Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                            intent.putExtra("UserId", UserId);
                            startActivity(intent);
                        }else {
                            //belum terdaftar otu dan oway
                            Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                            intent.putExtra("UserId", UserId);
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseCheckMemberExist> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FirebaseToken response) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(FirebaseToken.class);
        realm.copyToRealm(response);
        realm.commitTransaction();
    }
}
