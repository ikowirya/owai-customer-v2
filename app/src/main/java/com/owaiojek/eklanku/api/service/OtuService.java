package com.owaiojek.eklanku.api.service;

import com.owaiojek.eklanku.model.BankOtu;
import com.owaiojek.eklanku.model.NewsOtu;
import com.owaiojek.eklanku.model.ResponseCheckOtu;
import com.owaiojek.eklanku.model.ResponseRegisterOtu;
import com.owaiojek.eklanku.model.ResponseTopUp;
import com.owaiojek.eklanku.model.SaldoBonusOtu;
import com.owaiojek.eklanku.model.json.news.NewsResponse;
import com.owaiojek.eklanku.model.json.user.ResponseCheckMemberExist;
import com.owaiojek.eklanku.model.json.user.profile.ResponseGetProfile;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Iko Wirya on 4/18/2019.
 */
public interface OtuService {
    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/go_register")
    Call<ResponseRegisterOtu> registerOtu(@Field("aplUse") String aplUse,
                                          @Field("email") String email,
                                          @Field("nama") String nama,
                                          @Field("password") String password,
                                          @Field("pin") String pin,
                                          @Field("uplineID") String uplineID,
                                          @Field("userID") String userID);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/login")
    Call<ResponseCheckOtu> loginOtu(@Field("userID") String userID,
                                    @Field("token") String token,
                                    @Field("securityCode") String securityCode,
                                    @Field("passwd") String passwd,
                                    @Field("aplUse") String aplUse,
                                    @Field("firebase_id") String firebaseId);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/get_saldo_bonus")
    Call<SaldoBonusOtu> saldoBonus(@Field("userID") String userID,
                                   @Field("accessToken") String accessToken,
                                   @Field("aplUse") String aplUse);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Deposit/bank")
    Call<BankOtu> getBank(@Field("userID") String userID,
                          @Field("accessToken") String accessToken,
                          @Field("aplUse") String aplUse);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Deposit/request")
    Call<ResponseTopUp> requestBank(@Field("userID") String userID,
                                    @Field("accessToken") String accessToken,
                                    @Field("aplUse") String aplUse,
                                    @Field("bank") String bank,
                                    @Field("nominal") String nominal);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/logout")
    Call<ResponseRegisterOtu> logout(@Field("userID") String userID,
                                     @Field("accessToken") String accessToken,
                                     @Field("token") String token,
                                     @Field("aplUse") String aplUse);

    @Headers("x-api-key: 222")
    @GET("Konten/news")
    Call<NewsOtu> requestNews();

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Konten/news_by_id")
    Call<NewsOtu> requestNewsDetail(@Field("id") String berita_id);
    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/get_member")
    Call<ResponseCheckMemberExist> getMember(@Field("userID") String userID,
                                             @Field("aplUse") String apiUse
    );

    @Headers("x-api-key: 222")
    @GET("Konten/news")
    Call<NewsResponse> getNews();

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/profile")
    Call<ResponseGetProfile> getProfile(@Field("userID") String userID,
                                        @Field("accessToken") String accessToken,
                                        @Field("aplUse") String aplUse);
}
