package com.owaiojek.eklanku.signIn;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.home.MainActivity;
import com.owaiojek.eklanku.model.FirebaseToken;
import com.owaiojek.eklanku.model.ResponseCheckOtu;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.user.LoginRequestJson;
import com.owaiojek.eklanku.model.json.user.LoginResponseJson;
import com.owaiojek.eklanku.model.json.user.RegisterRequestJson;
import com.owaiojek.eklanku.model.json.user.RegisterResponseJson;
import com.owaiojek.eklanku.model.json.user.SendOtuResponseJson;
import com.owaiojek.eklanku.model.json.user.SendTokenJson;
import com.owaiojek.eklanku.model.json.user.profile.DataProfile;
import com.owaiojek.eklanku.model.json.user.profile.ResponseGetProfile;
import com.owaiojek.eklanku.signUp.SignUpActivity;
import com.owaiojek.eklanku.utils.DialogActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bradhawk on 10/12/2016.
 */

public class SignInActivity extends DialogActivity /*implements Validator.ValidationListener*/ {

    private static final String TAG = "SignInActivity";

    @NotEmpty
    @Email
    @BindView(R.id.signIn_email)
    EditText textEmail;

    @NotEmpty
    @BindView(R.id.signIn_password)
    EditText textPassword;

    @BindView(R.id.signIn_signInButton)
    Button buttonSignIn;

    /*@BindView(R.id.txtRegister)
    LinearLayout buttonSignUp;*/

    @BindView(R.id.txtRegister)
    TextView txtRegister;

    Validator validator;

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    OtuService otuService;
    String id_customer;
    String UserId, email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        otuService = OtuApi.getClient().create(OtuService.class);

        UserId = getIntent().getStringExtra("UserId");
        email = getIntent().getStringExtra("Email");

//        validator = new Validator(this);
//        validator.setValidationListener(this);

//        txtRegister.setPaintFlags(txtRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//
//        txtRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
//                startActivityForResult(intent, SignUpActivity.SIGNUP_ID);
//            }
//        });

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validator.validate();
                if (email != null) {
                    onSignInClick(email, textPassword.getText().toString());
                }else {
                    prepareLoginOtu();
                }
            }
        });

       /* textEmail.setText("ikowirya@gmail.com");
        textPassword.setText("123456");*/
    }


    private void onSignInClick(String email, String password) {
        Log.d("Suko","onSignInClick");
        showProgressDialog(R.string.dialog_loading);
        LoginRequestJson request = new LoginRequestJson();
        //request.setEmail(textEmail.getText().toString());
        request.setEmail(email);
        request.setPassword(password);

        Realm realm = Realm.getDefaultInstance();
        FirebaseToken token = realm.where(FirebaseToken.class).findFirst();
        if (token.getTokenId() != null) {
            request.setRegId(token.getTokenId());
        } else {
            Toast.makeText(this, R.string.waiting_pleaseWait, Toast.LENGTH_SHORT).show();
            hideProgressDialog();
            return;
        }

        UserService service = ServiceGenerator.createService(UserService.class, request.getEmail(), request.getPassword());
        service.login(request).enqueue(new Callback<LoginResponseJson>() {
            @Override
            public void onResponse(Call<LoginResponseJson> call, Response<LoginResponseJson> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("found")) {
                        User user = response.body().getData().get(0);
                        String userID = user.getNoTelepon();
                        id_customer  = user.getId();
                        String password = textPassword.getText().toString();
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        int minute = calendar.get(Calendar.MINUTE);
                        int second = calendar.get(Calendar.SECOND);
                        int date = calendar.get(Calendar.DAY_OF_MONTH);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        String tgl = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(date);
                        String time = String.valueOf(hour)+":"+String.valueOf(minute)+":"+String.valueOf(second);
                        String token = tgl+"T"+time;
                        String ecrpt = md5(password+"x@2564D");
                        String ecrpt2 = md5(ecrpt);
                        String securityCode = md5(token+ecrpt2);
                        loginOtu(userID,token,securityCode,password,"OWAICUSTOMER",request.getRegId(),true);
                        saveUser(user);

//                        Toast.makeText(SignInActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();


                    } else {
                        Toast.makeText(SignInActivity.this, "Username atau Password salah", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponseJson> call, Throwable t) {
                hideProgressDialog();
                t.printStackTrace();
                Toast.makeText(SignInActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void prepareLoginOtu(){
        Realm realm = Realm.getDefaultInstance();
        String fireBaseId;
        FirebaseToken tokenFcm = realm.where(FirebaseToken.class).findFirst();
        if (tokenFcm.getTokenId() != null) {
             fireBaseId = tokenFcm.getTokenId();
        } else {
            Toast.makeText(this, R.string.waiting_pleaseWait, Toast.LENGTH_SHORT).show();
            hideProgressDialog();
            return;
        }
        Log.d("Suko","prepareLoginOtu");
        String password = textPassword.getText().toString();
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        String tgl = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(date);
        String time = String.valueOf(hour)+":"+String.valueOf(minute)+":"+String.valueOf(second);
        String token = tgl+"T"+time;
        String ecrpt = md5(password+"x@2564D");
        String ecrpt2 = md5(ecrpt);
        String securityCode = md5(token+ecrpt2);
        loginOtu(UserId, token, securityCode, password,"OWAICUSTOMER",fireBaseId,false);

    }


    private void loginOtu(String valueUserID,final String token,String securityCode,String valuePassword,String valueAplUse,String firebaseId, boolean isRegisteredOnOwai) {
        Log.d("Suko","loginOtu");
        retrofit2.Call<ResponseCheckOtu> users = otuService.loginOtu(valueUserID,token,securityCode,valuePassword,valueAplUse,firebaseId);
        users.enqueue(new Callback<ResponseCheckOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseCheckOtu> call, retrofit2.Response<ResponseCheckOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        String mbr_token =  response.body().getMbr_token();
                        String eKL = response.body().getUserID();

                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref",0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("mbr_token",mbr_token);
                        editor.putString("userID",eKL);
                        editor.putString("token",token);
                        editor.commit();
                        if (isRegisteredOnOwai) {
                            sendToken(mbr_token, id_customer);
                        }else {
                            getProfileOtu(eKL, mbr_token,valuePassword,valueUserID);
                        }
                    }

                    else {
                        //Snackbar.make(findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), ""+response.body().getRespMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseCheckOtu> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getProfileOtu(String UserId,String accessToken,String valuePasswd, String valueUserId){
        Log.d("Suko","getProfileOtu");
        retrofit2.Call<ResponseGetProfile> call = otuService.getProfile(UserId,accessToken,"OWAICUSTOMER");
        call.enqueue(new Callback<ResponseGetProfile>() {
            @Override
            public void onResponse(Call<ResponseGetProfile> call, Response<ResponseGetProfile> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("SUCCESS")) {
                        ResponseGetProfile res = response.body();
                        DataProfile data = res.getData().get(0);
                        RegisterRequestJson request = new RegisterRequestJson();
                        request.setNamaDepan(data.getoNamaMember());
                        request.setNamaBelakang("");
                        request.setEmail(data.getoMail());
                        request.setPassword(valuePasswd);
                        request.setNoTelepon(valueUserId);
                        request.setAlamat(data.getoAlamat());
                        request.setFoto("");
                        request.setMbr_code(res.getUserID());

                        Realm realm = Realm.getDefaultInstance();
                        FirebaseToken token = realm.where(FirebaseToken.class).findFirst();
                        com.owaiojek.eklanku.utils.Log.e(TAG, "onSignUpClick: " + token);
                        if (token != null) {
                            request.setRegId(token.getTokenId());
                        }

                        owaiRegister(request);
                    }else{
                        Toast.makeText(SignInActivity.this, ""+response.body().getRespMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseGetProfile> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void owaiRegister(RegisterRequestJson request){
        Log.d("Suko","owaiRegister");
        UserService service = ServiceGenerator.createService(UserService.class, request.getEmail(), request.getPassword());
        service.register(request).enqueue(new Callback<RegisterResponseJson>() {
            @Override
            public void onResponse(Call<RegisterResponseJson> call, Response<RegisterResponseJson> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Register success")) {
//                        Intent retIntent = getIntent();
//                        retIntent.putExtra(USER_KEY, response.body().getData().get(0));
//                        setResult(Activity.RESULT_OK, retIntent);
                        Toast.makeText(getApplicationContext(), "Register sukses", Toast.LENGTH_LONG).show();
                        onSignInClick(request.getEmail(),request.getPassword());
                        finish();
                    } else {
                        //android.util.Log.d(TAG, "onResponse: ."+response.body().getData().get(0));

                        Toast.makeText(getApplicationContext(), "Register gagal", Toast.LENGTH_LONG).show();
                    }
                    hideProgressDialog();
                } else {
                    Toast.makeText(getApplicationContext(), "System error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseJson> call, Throwable t) {
                hideProgressDialog();
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sendToken(String access_token, String id_customer){
        Log.d("Suko","sendToken");
        SendTokenJson request = new SendTokenJson();
        request.access_token = access_token;
        request.id_customer = id_customer;

        UserService service = ServiceGenerator.createService(UserService.class, textEmail.getText().toString(),
                textPassword.getText().toString());
        service.sendTokenOtu(request).enqueue(new Callback<SendOtuResponseJson>() {
            @Override
            public void onResponse(Call<SendOtuResponseJson> call, Response<SendOtuResponseJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("success")) {

                            Toast.makeText(SignInActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SendOtuResponseJson> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SignUpActivity.SIGNUP_ID) {
            if (resultCode == Activity.RESULT_OK) {
                User user = (User) data.getSerializableExtra(SignUpActivity.USER_KEY);

                saveUser(user);

                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

//    @Override
//    public void onValidationSucceeded() {
//        onSignInClick();
//    }
//
//    @Override
//    public void onValidationFailed(List<ValidationError> errors) {
//        for (ValidationError error : errors) {
//            View view = error.getView();
//            String message = error.getCollatedErrorMessage(this);
//
//            if (view instanceof EditText) {
//                ((EditText) view).setError(message);
//            } else {
//                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
//            }
//        }
//    }

    private void saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(User.class);
        realm.copyToRealm(user);
        realm.commitTransaction();

        MangJekApplication.getInstance(SignInActivity.this).setLoginUser(user);
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FirebaseToken response) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(FirebaseToken.class);
        realm.copyToRealm(response);
        realm.commitTransaction();
    }
}
