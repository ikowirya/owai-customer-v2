package com.owaiojek.eklanku.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.home.HotNewsActivity;
import com.owaiojek.eklanku.mMassage.MassageActivity;
import com.owaiojek.eklanku.mService.mServiceActivity;
import com.owaiojek.eklanku.model.NewsOtuResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Iko Wirya on 5/22/2019.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    List<NewsOtuResponse> newsOtuResponseList;
    Context context;
    public NewsAdapter(List<NewsOtuResponse> newsOtuResponseList) {
        this.newsOtuResponseList = newsOtuResponseList;
    }

    @Override
    public NewsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        NewsAdapter.MyViewHolder viewHolder = new NewsAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.MyViewHolder holder, int i) {
        context = holder.itemView.getContext();
        String url = newsOtuResponseList.get(i).getBerita_gambar();
        Glide.with(context)
                .load(url)
                .thumbnail(0.5f)
                .into(holder.imgNews);
        holder.txtContent.setText(newsOtuResponseList.get(i).getBerita_isi());
        holder.txtTitle.setText(newsOtuResponseList.get(i).getBerita_judul());
        holder.btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HotNewsActivity.class);
                intent.putExtra("berita_id", newsOtuResponseList.get(i).getBerita_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsOtuResponseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgNews)
        ImageView imgNews;

        @BindView(R.id.txtContent)
        TextView txtContent;

        @BindView(R.id.txtTitle)
        TextView txtTitle;

        @BindView(R.id.btnRead)
        TextView btnRead;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
