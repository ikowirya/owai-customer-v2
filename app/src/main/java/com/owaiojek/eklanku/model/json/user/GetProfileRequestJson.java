package com.owaiojek.eklanku.model.json.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.owaiojek.eklanku.model.Banner;

import java.util.ArrayList;

/**
 * Created by Iko Wirya on 4/20/2019.
 */
public class GetProfileRequestJson {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("data")
    @Expose
    public ArrayList<UpdateProfileRequestJson> data;

}
