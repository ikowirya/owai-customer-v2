package com.owaiojek.eklanku.model.json.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 5/3/2019.
 */
public class UpdateSaldoOwaiOtuJson {
    @SerializedName("id_customer")
    @Expose
    public String id_customer;

    @SerializedName("nominal")
    @Expose
    public String nominal;
}
