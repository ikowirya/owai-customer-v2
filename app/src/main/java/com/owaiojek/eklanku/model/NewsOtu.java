package com.owaiojek.eklanku.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Iko Wirya on 5/22/2019.
 */
public class NewsOtu {
    @SerializedName("errNumber")
    private String errNumber;
    @SerializedName("userID")
    private String userID;
    @SerializedName("data")
    private List<NewsOtuResponse> data;
    @SerializedName("respTime")
    private String respTime;
    @SerializedName("status")
    private String status;
    @SerializedName("respMessage")
    private String respMessage;

    public NewsOtu(String errNumber, String userID, List<NewsOtuResponse> data, String respTime, String status, String respMessage) {
        this.errNumber = errNumber;
        this.userID = userID;
        this.data = data;
        this.respTime = respTime;
        this.status = status;
        this.respMessage = respMessage;
    }

    public String getErrNumber() {

        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<NewsOtuResponse> getData() {
        return data;
    }

    public void setData(List<NewsOtuResponse> data) {
        this.data = data;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
