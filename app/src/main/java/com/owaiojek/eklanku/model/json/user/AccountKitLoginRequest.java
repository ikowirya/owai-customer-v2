package com.owaiojek.eklanku.model.json.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountKitLoginRequest {

    @SerializedName("customer_phone")
    @Expose
    private String customer_phone;
    @SerializedName("token")
    @Expose
    private String token;

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
