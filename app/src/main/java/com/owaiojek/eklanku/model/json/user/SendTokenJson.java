package com.owaiojek.eklanku.model.json.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 5/2/2019.
 */
public class SendTokenJson {
    @SerializedName("access_token")
    @Expose
    public String access_token;

    @SerializedName("id_customer")
    @Expose
    public String id_customer;


}
