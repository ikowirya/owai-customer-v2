package com.owaiojek.eklanku.model.json.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataProfile {
    @SerializedName("o_id_sponsor")
    @Expose
    private String oIdSponsor;
    @SerializedName("o_nama_sponsor")
    @Expose
    private String oNamaSponsor;
    @SerializedName("o_nama_member")
    @Expose
    private String oNamaMember;
    @SerializedName("o_hp")
    @Expose
    private String oHp;
    @SerializedName("o_mail")
    @Expose
    private String oMail;
    @SerializedName("o_alamat")
    @Expose
    private String oAlamat;
    @SerializedName("o_kota")
    @Expose
    private String oKota;
    @SerializedName("o_tgl_lahir")
    @Expose
    private String oTglLahir;
    @SerializedName("o_tgl_daftar")
    @Expose
    private String oTglDaftar;
    @SerializedName("o_bank")
    @Expose
    private String oBank;
    @SerializedName("o_norec")
    @Expose
    private String oNorec;
    @SerializedName("o_pemilikrekening")
    @Expose
    private String oPemilikrekening;
    @SerializedName("o_jabatan_sponsor")
    @Expose
    private String oJabatanSponsor;
    @SerializedName("o_jabatanmember")
    @Expose
    private String oJabatanmember;
    @SerializedName("o_hp_sponsor")
    @Expose
    private String oHpSponsor;
    @SerializedName("no_ktp")
    @Expose
    private String noKtp;

    public String getoIdSponsor() {
        return oIdSponsor;
    }

    public void setoIdSponsor(String oIdSponsor) {
        this.oIdSponsor = oIdSponsor;
    }

    public String getoNamaSponsor() {
        return oNamaSponsor;
    }

    public void setoNamaSponsor(String oNamaSponsor) {
        this.oNamaSponsor = oNamaSponsor;
    }

    public String getoNamaMember() {
        return oNamaMember;
    }

    public void setoNamaMember(String oNamaMember) {
        this.oNamaMember = oNamaMember;
    }

    public String getoHp() {
        return oHp;
    }

    public void setoHp(String oHp) {
        this.oHp = oHp;
    }

    public String getoMail() {
        return oMail;
    }

    public void setoMail(String oMail) {
        this.oMail = oMail;
    }

    public String getoAlamat() {
        return oAlamat;
    }

    public void setoAlamat(String oAlamat) {
        this.oAlamat = oAlamat;
    }

    public String getoKota() {
        return oKota;
    }

    public void setoKota(String oKota) {
        this.oKota = oKota;
    }

    public String getoTglLahir() {
        return oTglLahir;
    }

    public void setoTglLahir(String oTglLahir) {
        this.oTglLahir = oTglLahir;
    }

    public String getoTglDaftar() {
        return oTglDaftar;
    }

    public void setoTglDaftar(String oTglDaftar) {
        this.oTglDaftar = oTglDaftar;
    }

    public String getoBank() {
        return oBank;
    }

    public void setoBank(String oBank) {
        this.oBank = oBank;
    }

    public String getoNorec() {
        return oNorec;
    }

    public void setoNorec(String oNorec) {
        this.oNorec = oNorec;
    }

    public String getoPemilikrekening() {
        return oPemilikrekening;
    }

    public void setoPemilikrekening(String oPemilikrekening) {
        this.oPemilikrekening = oPemilikrekening;
    }

    public String getoJabatanSponsor() {
        return oJabatanSponsor;
    }

    public void setoJabatanSponsor(String oJabatanSponsor) {
        this.oJabatanSponsor = oJabatanSponsor;
    }

    public String getoJabatanmember() {
        return oJabatanmember;
    }

    public void setoJabatanmember(String oJabatanmember) {
        this.oJabatanmember = oJabatanmember;
    }

    public String getoHpSponsor() {
        return oHpSponsor;
    }

    public void setoHpSponsor(String oHpSponsor) {
        this.oHpSponsor = oHpSponsor;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }
}
