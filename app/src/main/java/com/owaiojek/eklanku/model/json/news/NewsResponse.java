package com.owaiojek.eklanku.model.json.news;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsResponse {
    @SerializedName("errNumber")
    @Expose
    private String errNumber;
    @SerializedName("userID")
    @Expose
    private String userID;
    @SerializedName("data")
    @Expose
    private List<News> data = null;
    @SerializedName("respTime")
    @Expose
    private String respTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("respMessage")
    @Expose
    private String respMessage;

    public String getErrNumber() {
        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<News> getData() {
        return data;
    }

    public void setData(List<News> data) {
        this.data = data;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
