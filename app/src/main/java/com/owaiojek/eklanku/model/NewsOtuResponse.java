package com.owaiojek.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 5/22/2019.
 */
public class NewsOtuResponse {
    @SerializedName("berita_id")
    private String berita_id;
    @SerializedName("berita_date")
    private String berita_date;
    @SerializedName("berita_status")
    private String berita_status;
    @SerializedName("berita_isi")
    private String berita_isi;
    @SerializedName("berita_judul")
    private String berita_judul;
    @SerializedName("berita_autor")
    private String berita_author;
    @SerializedName("berita_gambar")
    private String berita_gambar;
    @SerializedName("berita_kode")
    private String berita_kode;
    @SerializedName("berita_detail")
    private String berita_detail;
    @SerializedName("respTime")
    private String respTime;
    @SerializedName("status")
    private String status;
    @SerializedName("respMessage")
    private String respMessage;

    public NewsOtuResponse(String berita_id, String berita_date, String berita_status,
                           String berita_isi, String berita_judul, String berita_author,
                           String berita_gambar, String berita_kode, String berita_detail, String respTime,
                           String status, String respMessage) {
        this.berita_id = berita_id;
        this.berita_date = berita_date;
        this.berita_status = berita_status;
        this.berita_isi = berita_isi;
        this.berita_judul = berita_judul;
        this.berita_author = berita_author;
        this.berita_gambar = berita_gambar;
        this.berita_kode = berita_kode;
        this.berita_detail = berita_detail;
        this.respTime = respTime;
        this.status = status;
        this.respMessage = respMessage;
    }

    public String getBerita_id() {

        return berita_id;
    }

    public void setBerita_id(String berita_id) {
        this.berita_id = berita_id;
    }

    public String getBerita_date() {
        return berita_date;
    }

    public void setBerita_date(String berita_date) {
        this.berita_date = berita_date;
    }

    public String getBerita_status() {
        return berita_status;
    }

    public void setBerita_status(String berita_status) {
        this.berita_status = berita_status;
    }

    public String getBerita_isi() {
        return berita_isi;
    }

    public void setBerita_isi(String berita_isi) {
        this.berita_isi = berita_isi;
    }

    public String getBerita_judul() {
        return berita_judul;
    }

    public void setBerita_judul(String berita_judul) {
        this.berita_judul = berita_judul;
    }

    public String getBerita_author() {
        return berita_author;
    }

    public void setBerita_author(String berita_author) {
        this.berita_author = berita_author;
    }

    public String getBerita_gambar() {
        return berita_gambar;
    }

    public void setBerita_gambar(String berita_gambar) {
        this.berita_gambar = berita_gambar;
    }

    public String getBerita_kode() {
        return berita_kode;
    }

    public void setBerita_kode(String berita_kode) {
        this.berita_kode = berita_kode;
    }

    public String getBerita_detail() {
        return berita_detail;
    }

    public void setBerita_detail(String berita_detail) {
        this.berita_detail = berita_detail;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
