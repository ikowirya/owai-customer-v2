package com.owaiojek.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/18/2019.
 */
public class ResponseTopUp {
    @SerializedName("errNumber")
    private String errNumber;
    @SerializedName("userID")
    private String userID;
    @SerializedName("bank")
    private String bank;
    @SerializedName("kode_bank")
    private String kode_bank;
    @SerializedName("logo")
    private String logo;
    @SerializedName("nominal")
    private String nominal;
    @SerializedName("nomer_rekening")
    private String nomer_rekening;
    @SerializedName("nama_pemilik")
    private String nama_pemilik;
    @SerializedName("respTime")
    private String respTime;
    @SerializedName("status")
    private String status;
    @SerializedName("respMessage")
    private String respMessage;

    public ResponseTopUp(String errNumber, String userID, String bank, String nominal, String respTime, String status, String respMessage) {
        this.errNumber = errNumber;
        this.userID = userID;
        this.bank = bank;
        this.nominal = nominal;
        this.respTime = respTime;
        this.status = status;
        this.respMessage = respMessage;
    }

    public String getErrNumber() {

        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getKode_bank() {
        return kode_bank;
    }

    public void setKode_bank(String kode_bank) {
        this.kode_bank = kode_bank;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNomer_rekening() {
        return nomer_rekening;
    }

    public void setNomer_rekening(String nomer_rekening) {
        this.nomer_rekening = nomer_rekening;
    }

    public String getNama_pemilik() {
        return nama_pemilik;
    }

    public void setNama_pemilik(String nama_pemilik) {
        this.nama_pemilik = nama_pemilik;
    }
}
