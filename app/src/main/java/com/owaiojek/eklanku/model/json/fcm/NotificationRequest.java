package com.owaiojek.eklanku.model.json.fcm;

/**
 * Created by Iko Wirya on 4/29/2019.
 */

import com.google.gson.annotations.SerializedName;

public class NotificationRequest {
    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("content_available")
    private boolean content = true;

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
