package com.owaiojek.eklanku.model.json.news;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News {
    @SerializedName("berita_id")
    @Expose
    private String beritaId;
    @SerializedName("berita_date")
    @Expose
    private String beritaDate;
    @SerializedName("berita_status")
    @Expose
    private String beritaStatus;
    @SerializedName("berita_isi")
    @Expose
    private String beritaIsi;
    @SerializedName("berita_judul")
    @Expose
    private String beritaJudul;
    @SerializedName("berita_autor")
    @Expose
    private String beritaAutor;
    @SerializedName("berita_gambar")
    @Expose
    private String beritaGambar;
    @SerializedName("berita_kode")
    @Expose
    private String beritaKode;
    @SerializedName("berita_detail")
    @Expose
    private String beritaDetail;

    public String getBeritaId() {
        return beritaId;
    }

    public void setBeritaId(String beritaId) {
        this.beritaId = beritaId;
    }

    public String getBeritaDate() {
        return beritaDate;
    }

    public void setBeritaDate(String beritaDate) {
        this.beritaDate = beritaDate;
    }

    public String getBeritaStatus() {
        return beritaStatus;
    }

    public void setBeritaStatus(String beritaStatus) {
        this.beritaStatus = beritaStatus;
    }

    public String getBeritaIsi() {
        return beritaIsi;
    }

    public void setBeritaIsi(String beritaIsi) {
        this.beritaIsi = beritaIsi;
    }

    public String getBeritaJudul() {
        return beritaJudul;
    }

    public void setBeritaJudul(String beritaJudul) {
        this.beritaJudul = beritaJudul;
    }

    public String getBeritaAutor() {
        return beritaAutor;
    }

    public void setBeritaAutor(String beritaAutor) {
        this.beritaAutor = beritaAutor;
    }

    public String getBeritaGambar() {
        return beritaGambar;
    }

    public void setBeritaGambar(String beritaGambar) {
        this.beritaGambar = beritaGambar;
    }

    public String getBeritaKode() {
        return beritaKode;
    }

    public void setBeritaKode(String beritaKode) {
        this.beritaKode = beritaKode;
    }

    public String getBeritaDetail() {
        return beritaDetail;
    }

    public void setBeritaDetail(String beritaDetail) {
        this.beritaDetail = beritaDetail;
    }
}
