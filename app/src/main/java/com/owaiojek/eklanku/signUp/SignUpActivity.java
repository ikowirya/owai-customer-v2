package com.owaiojek.eklanku.signUp;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import com.owaiojek.eklanku.MangJekApplication;
import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.ServiceGenerator;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.api.service.UserService;
import com.owaiojek.eklanku.home.MainActivity;
import com.owaiojek.eklanku.model.FirebaseToken;
import com.owaiojek.eklanku.model.ResponseCheckOtu;
import com.owaiojek.eklanku.model.ResponseRegisterOtu;
import com.owaiojek.eklanku.model.User;
import com.owaiojek.eklanku.model.json.user.LoginRequestJson;
import com.owaiojek.eklanku.model.json.user.LoginResponseJson;
import com.owaiojek.eklanku.model.json.user.RegisterRequestJson;
import com.owaiojek.eklanku.model.json.user.RegisterResponseJson;
import com.owaiojek.eklanku.model.json.user.SendOtuResponseJson;
import com.owaiojek.eklanku.model.json.user.SendTokenJson;
import com.owaiojek.eklanku.utils.DialogActivity;
import com.owaiojek.eklanku.utils.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bradhawk on 10/12/2016.
 */

public class SignUpActivity extends DialogActivity implements Validator.ValidationListener {

    public static final int SIGNUP_ID = 110;
    public static final String USER_KEY = "UserKey";
    private static final String TAG = "SignUpActivity";
    @NotEmpty
    @BindView(R.id.signUp_firstName)
    EditText textFirstName;

    @NotEmpty
    @BindView(R.id.signUp_lastName)
    EditText textLastName;

    @NotEmpty
    @BindView(R.id.txtPin)
    EditText txtPin;

    @NotEmpty
    @BindView(R.id.txtUplineID)
    EditText txtUplineID;

    @NotEmpty
    @Email
    @BindView(R.id.signUp_email)
    EditText textEmail;

    @NotEmpty
    @Password
    @BindView(R.id.signUp_password)
    EditText textPassword;

    @NotEmpty
    @BindView(R.id.signUp_phone)
    EditText textPhone;

    @NotEmpty
    @BindView(R.id.signUp_address)
    EditText textAddress;

    @BindView(R.id.img_profile)
    CircleImageView img_profile;

//    @NotEmpty
//    @BindView(R.id.signUp_pob)
//    EditText textPlaceOfBirth;
//
//    @NotEmpty
//    @BindView(R.id.signUp_dob)
//    EditText textDateOfBirth;

    @BindView(R.id.signUp_signUpButton)
    Button buttonSignUp;

    @BindView(R.id.txtLogin)
    TextView txtLogin;

    private byte[] bytes;
    String hasilFoto=null;

/*    @BindView(R.id.signUp_signInButton)
    LinearLayout buttonSignIn;*/

    Calendar calendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private Validator validator;

    private void updateLabel() {
        String format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
//        textDateOfBirth.setText(sdf.format(calendar.getTime()));
    }

    private void showDatePicker() {
        new DatePickerDialog(this, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    String valueNama,valueEmail,valuePassword,valuePin,valueUplineID,valueUserID,eklOtu;
    OtuService otuService;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        otuService = OtuApi.getClient().create(OtuService.class);
        validator = new Validator(this);
        validator.setValidationListener(this);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.facebook);
        new Thread(new Runnable() {
            @Override
            public void run() {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                byte[] imageBytes = baos.toByteArray();
                hasilFoto = "data:image/jpeg;base64,"+Base64.encodeToString(imageBytes, Base64.DEFAULT);
            }
        });


        if (getIntent().getStringExtra("UserId") != null){
            textPhone.setVisibility(View.GONE);
            textPhone.setText(getIntent().getStringExtra("UserId"));
        }

        txtLogin.setPaintFlags(txtLogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        try {
            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validator.validate();
                }
            });
        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        textDateOfBirth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (b) showDatePicker();
//            }
//        });
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCameraPermission(SignUpActivity.this)) {
                    selectPicture();
                }



            }
        });
    }
    public static boolean checkCameraPermission(Activity context) {
        int cameraCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);

        int writeExternalCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (cameraCheck == PackageManager.PERMISSION_GRANTED && writeExternalCheck == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(context,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            return false;
        }
    }


    private void selectPicture(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 0);
    }

    private byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return baos.toByteArray();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                try {
                    InputStream imageStream = SignUpActivity.this.getContentResolver().openInputStream(selectedImage);
                    bytes = convertBitmapToByteArray(BitmapFactory.decodeStream(imageStream));
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    img_profile.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                    hasilFoto = "data:image/jpeg;base64,"+Base64.encodeToString(bytes, Base64.DEFAULT);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }


    }

    private void onSignUpClick() {
        valueNama = textFirstName.getText().toString() +" "+textLastName.getText().toString();
        valueEmail = textEmail.getText().toString();
        valuePassword = textPassword.getText().toString();
        valueUserID = textPhone.getText().toString();
        valuePin = txtPin.getText().toString();
        valueUplineID = txtUplineID.getText().toString();
        if (!valueNama.isEmpty()||!valuePassword.isEmpty()||!valueUplineID.isEmpty()||!valuePin.isEmpty()
                ||!valueUserID.isEmpty()||!valueEmail.isEmpty()||hasilFoto!=null)
        {
            if (valuePassword.matches(".*[A-Za-z].*") && valuePassword.matches(".*[0-9].*")
                    && valuePassword.matches("[A-Za-z0-9]*"))
            {
                registerOtu("OWAICUSTOMER",valueEmail,valueNama,valuePassword,valuePin,"EKL"+valueUplineID,valueUserID);
            }
            else {
                Snackbar.make(findViewById(R.id.main), "Password, Its NOT Alphanumeric!", Snackbar.LENGTH_SHORT).show();
            }

        }else {
            Snackbar.make(findViewById(R.id.main), "Please, Completed Form Data!", Snackbar.LENGTH_SHORT).show();
        }

    }


    private void registerOtu(String valueAplUse,String valueEmail, String valueNama, String valuePassword,
                             String valuePin,String valueUplineID,String valueUserID) {
        showProgressDialog(R.string.dialog_loading);
        retrofit2.Call<ResponseRegisterOtu> users = otuService.registerOtu(valueAplUse,valueEmail,valueNama,valuePassword,valuePin,valueUplineID,valueUserID);
        users.enqueue(new Callback<ResponseRegisterOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseRegisterOtu> call, retrofit2.Response<ResponseRegisterOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        eklOtu =  response.body().getEkl();
                        owaiRegister();

                    }else if (response.body().getStatus().equals("FAILED"))
                    {
                        Toast.makeText(SignUpActivity.this, ""+response.body().getRespMessage(), Toast.LENGTH_SHORT).show();
                    }
//                    else if (response.body().getRespMessage().equals("YOUR PHONE NUMBER IS ALREADY REGISTERED IN OUR SYSTEM")|response.body().getRespMessage().equals("THIS EMAIL ALREADY REGISTER IN CURRENT SERVER")){
//                        //owaiRegister();
//                    }
                    else {
                        Snackbar.make(findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
                hideProgressDialog();
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseRegisterOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                hideProgressDialog();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void owaiRegister(){
        showProgressDialog(R.string.dialog_loading);
        RegisterRequestJson request = new RegisterRequestJson();
        request.setNamaDepan(textFirstName.getText().toString());
        request.setNamaBelakang(textLastName.getText().toString());
        request.setEmail(textEmail.getText().toString());
        request.setPassword(textPassword.getText().toString());
        request.setNoTelepon(textPhone.getText().toString());
        request.setAlamat(textAddress.getText().toString());
        request.setFoto(hasilFoto);
        request.setMbr_code(eklOtu);
//        request.setTempatLahir(textPlaceOfBirth.getText().toString());
//        request.setTglLahir(textDateOfBirth.getText().toString());

        Realm realm = Realm.getDefaultInstance();
        FirebaseToken token = realm.where(FirebaseToken.class).findFirst();
        Log.e(TAG, "onSignUpClick: " + token);
        if (token != null) {
            request.setRegId(token.getTokenId());
        }

        UserService service = ServiceGenerator.createService(UserService.class, request.getEmail(), request.getPassword());
        service.register(request).enqueue(new Callback<RegisterResponseJson>() {
            @Override
            public void onResponse(Call<RegisterResponseJson> call, Response<RegisterResponseJson> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Register success")) {
//                        Intent retIntent = getIntent();
//                        retIntent.putExtra(USER_KEY, response.body().getData().get(0));
//                        setResult(Activity.RESULT_OK, retIntent);
                        Toast.makeText(SignUpActivity.this, "Register sukses", Toast.LENGTH_LONG).show();
                        onSignInClick(request.getEmail(),request.getPassword());
                        finish();
                    } else {
//                        android.util.Log.d(TAG, "onResponse: ."+response.body().getData().get(0).);
                        Toast.makeText(SignUpActivity.this, "Register gagal", Toast.LENGTH_LONG).show();
                    }
                    hideProgressDialog();
                } else {
                    Toast.makeText(SignUpActivity.this, "System error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseJson> call, Throwable t) {
                hideProgressDialog();
                t.printStackTrace();
                Toast.makeText(SignUpActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onValidationSucceeded() {
        onSignUpClick();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onSignInClick(String email,String pass) {
        //showProgressDialog(R.string.dialog_loading);
        LoginRequestJson request = new LoginRequestJson();
        //request.setEmail(textEmail.getText().toString());
        request.setEmail(email);
        request.setPassword(pass);

        Realm realm = Realm.getDefaultInstance();
        FirebaseToken token = realm.where(FirebaseToken.class).findFirst();
        if (token.getTokenId() != null) {
            request.setRegId(token.getTokenId());
        } else {
            Toast.makeText(this, R.string.waiting_pleaseWait, Toast.LENGTH_SHORT).show();
            //hideProgressDialog();
            return;
        }

        UserService service = ServiceGenerator.createService(UserService.class, request.getEmail(), request.getPassword());
        service.login(request).enqueue(new Callback<LoginResponseJson>() {
            @Override
            public void onResponse(Call<LoginResponseJson> call, Response<LoginResponseJson> response) {
                //hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("found")) {
                        User user = response.body().getData().get(0);
                        String userID = user.getNoTelepon();
                        String id_customer  = user.getId();
                        String password = request.getPassword();
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        int minute = calendar.get(Calendar.MINUTE);
                        int second = calendar.get(Calendar.SECOND);
                        int date = calendar.get(Calendar.DAY_OF_MONTH);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        String tgl = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(date);
                        String time = String.valueOf(hour)+":"+String.valueOf(minute)+":"+String.valueOf(second);
                        String token = tgl+"T"+time;
                        String ecrpt = md5(password+"x@2564D");
                        String ecrpt2 = md5(ecrpt);
                        String securityCode = md5(token+ecrpt2);
                        loginOtu(userID,token,securityCode,password,id_customer,email,"OWAICUSTOMER",request.getRegId());
                        saveUser(user);

//                        Toast.makeText(SignInActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();


                    } else {
                        Toast.makeText(SignUpActivity.this, "Username atau Password salah", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponseJson> call, Throwable t) {
                //hideProgressDialog();
                t.printStackTrace();
                Toast.makeText(SignUpActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void loginOtu(String valueUserID,final String token,String securityCode,String valuePassword,String id_customer,String email, String valueAplUse,String firebaseId) {
        retrofit2.Call<ResponseCheckOtu> users = otuService.loginOtu(valueUserID,token,securityCode,valuePassword,valueAplUse,firebaseId);
        users.enqueue(new Callback<ResponseCheckOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseCheckOtu> call, retrofit2.Response<ResponseCheckOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        String mbr_token =  response.body().getMbr_token();
                        String userID = response.body().getUserID();
                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref",0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("mbr_token",mbr_token);
                        editor.putString("userID",userID);
                        editor.putString("token",token);
                        editor.commit();
                        sendToken(mbr_token, id_customer,email,valuePassword);

                    }

                    else {
                        Snackbar.make(findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseCheckOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendToken(String access_token, String id_customer, String email, String pass){

        SendTokenJson request = new SendTokenJson();
        request.access_token = access_token;
        request.id_customer = id_customer;

        UserService service = ServiceGenerator.createService(UserService.class, email, pass);
        service.sendTokenOtu(request).enqueue(new Callback<SendOtuResponseJson>() {
            @Override
            public void onResponse(Call<SendOtuResponseJson> call, Response<SendOtuResponseJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("success")) {

                        Toast.makeText(SignUpActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SendOtuResponseJson> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "System error: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(User.class);
        realm.copyToRealm(user);
        realm.commitTransaction();

        MangJekApplication.getInstance(SignUpActivity.this).setLoginUser(user);
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FirebaseToken response) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(FirebaseToken.class);
        realm.copyToRealm(response);
        realm.commitTransaction();
    }
}
