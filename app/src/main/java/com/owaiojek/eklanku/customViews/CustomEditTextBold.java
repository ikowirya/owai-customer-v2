package com.owaiojek.eklanku.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.owaiojek.eklanku.R;


public class CustomEditTextBold extends AppCompatEditText {

    private Context context;

    public CustomEditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        if (!isInEditMode())
            init();
    }
    private void init() {
        Typeface font = Typeface.createFromAsset(context.getAssets(), getResources().getString(R.string.main_font));
        setTypeface(font, Typeface.BOLD);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf);
    }

}
