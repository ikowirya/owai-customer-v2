package com.owaiojek.eklanku.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.owaiojek.eklanku.R;


public class CustomButton extends AppCompatButton {

    private Context context;

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        if (!isInEditMode())
            init();
    }

    private void init() {
        Typeface font = Typeface.createFromAsset(context.getAssets(), getResources().getString(R.string.main_font));
        setTypeface(font, Typeface.NORMAL);
        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Drawable drawable = VectorDrawableUtils.getDrawable(context, outValue.resourceId);
            setForeground(drawable);
        }*/
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf);
    }

}
